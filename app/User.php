<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Notifications\MailResetPasswordNotification;
use Auth;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'ponsel'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function user() {
        return $this->hasOne("App\ClassAkun","id","id");
    }

    // Adhoc Method : saat ini hanya dipakai untuk manage tampilan
    public function getMyRole() {
        $role = new \App\ClassRole;
        return $role->getMyRole(Auth::user()->id);
    }

    public function isAdmin() {
        $getMyRole = $this->getMyRole();
        $my_roles  = [];
        foreach ($getMyRole as $key => $value) {
            array_push($my_roles, $value->id_role);
        };
        $admin_roles = VarGlobal::$array_admin;
        $is_admin = FungsiGlobal::array_in_array($my_roles,$admin_roles);
        return $is_admin;
    }

    public function customer_message() {
        return $this->hasMany("App\Model\Master\customer_message","id_customer","id");
    }

    public function unpayed_request() {
        $count = \App\Model\Master\permohonan::where("id_pemohon",Auth::user()->id)
                    ->where("pembayaran_dikonfirmasi",false)
                    ->count();
        return $count;
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new MailResetPasswordNotification($token));
    }
}
