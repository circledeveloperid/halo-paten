<?php

namespace App\Model\Menu;

use Illuminate\Database\Eloquent\Model;

class MenuHeader extends Model
{
    protected $table = "menu_header";
    protected $primaryKey = "id_header";

    public function menu_parent() {
    	return $this->hasMany("App\Model\Menu\MenuParent","id_header","id_header");
    }
}
