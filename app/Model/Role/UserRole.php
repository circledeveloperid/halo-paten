<?php

namespace App\Model\Role;

use Illuminate\Database\Eloquent\Model;

class UserRole extends Model
{
    protected $table = "user_role";

    public function role() {
    	return $this->belongsTo("App\Model\Role\Role","id_role","id");
    }
}
