<?php

namespace App\Model\Master;

use Illuminate\Database\Eloquent\Model;

class status_permohonan extends Model
{
    protected $table = "status_permohonan";
    protected $primaryKey = "id_status_permohonan";
    public $timestamps = false;

    public function permohonan() {
    	return $this->hasMany('App\Model\Master\permohonan',"id_status_permohonan","id_status_permohonan");
    }

    public function get_id_status_permohonan($id_permohonan) {
    	$permohonan = App\Model\Master\permohonan::where("id_permohonan",$id_permohonan)->select("id_status_permohonan")->first();
    	if($permohonan)
    		return $permohonan->id_status_permohonan;
    	else
    		return 0;
    }
}
