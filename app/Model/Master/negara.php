<?php

namespace App\Model\Master;

use Illuminate\Database\Eloquent\Model;

class negara extends Model
{
    protected $table = "negara";
    protected $primaryKey = "id";
    public $timestamps = false;

    public function provinsi() {
    	return $this->hasMany('App\Model\Master\provinsi',"country_id","id");
    }

    public function kota() {
    	return $this->hasMany('App\Model\Master\kota',"country_id","id");
    }
}
