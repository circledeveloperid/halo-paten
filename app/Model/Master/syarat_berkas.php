<?php

namespace App\Model\Master;

use Illuminate\Database\Eloquent\Model;

class syarat_berkas extends Model
{
    protected $table = "syarat_berkas";
    protected $primaryKey = "id_syarat_berkas";
    public $timestamps = false;

    public function data_berkas() {
    	return $this->hasMany('App\Model\Master\data_berkas',"id_syarat_berkas","id_syarat_berkas");
    }

    public function jenis_pemohon() {
    	return $this->hasMany('App\Model\Master\jenis_pemohon',"id_jenis_pemohon","id_jenis_pemohon");
    }
}
