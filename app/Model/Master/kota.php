<?php

namespace App\Model\Master;

use Illuminate\Database\Eloquent\Model;

class kota extends Model
{
    protected $table = "kota";
    protected $primaryKey = "id";
    public $timestamps = false;

    public function data_pemohon() {
    	return $this->hasMany('App\Model\Master\data_pemohon',"id_kota","id");
    }

    public function provinsi() {
    	return $this->belongsTo('App\Model\Master\provinsi',"state_id","id");
    }

    public function negara() {
    	return $this->belongsTo('App\Model\Master\negara',"country_id","id");
    }
}
