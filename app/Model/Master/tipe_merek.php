<?php

namespace App\Model\Master;

use Illuminate\Database\Eloquent\Model;

class tipe_merek extends Model
{
    protected $table = "tipe_merek";
    protected $primaryKey = "id_tipe_merek";
    public $timestamps = false;

    public function data_merek() {
    	return $this->hasMany('App\Model\Master\data_merek',"id_tipe_merek","id_tipe_merek");
    }
}
