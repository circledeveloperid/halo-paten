<?php

namespace App\Model\Master;

use Illuminate\Database\Eloquent\Model;

class permohonan extends Model
{
    protected $table = "permohonan";
    protected $primaryKey = "id_permohonan";
    public $timestamps = false;

    public function pemohon() {
    	return $this->belongsTo('App\User',"id_pemohon","id");
    }

    public function status_permohonan() {
        return $this->belongsTo('App\Model\Master\status_permohonan',"id_status_permohonan","id_status_permohonan");
    }

    public function data_pemohon_lainnya() {
    	return $this->hasMany('App\Model\Master\data_pemohon_lainnya',"id_permohonan","id_permohonan");
    }

    public function data_merek() {
    	return $this->hasOne('App\Model\Master\data_merek',"id_permohonan","id_permohonan");
    }

    public function data_kelas() {
    	return $this->hasMany('App\Model\Master\data_kelas',"id_permohonan","id_permohonan");
    }

    public function data_berkas() {
    	return $this->hasMany('App\Model\Master\data_berkas',"id_permohonan","id_permohonan");
    }

    public function data_pemohon() {
    	return $this->hasOne('App\Model\Master\data_pemohon',"id_permohonan","id_permohonan");
    }
}
