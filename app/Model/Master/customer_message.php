<?php

namespace App\Model\Master;

use Illuminate\Database\Eloquent\Model;

class customer_message extends Model
{
    protected $table = "customer_message";
    protected $primaryKey = "id_chat";

    public function customer() {
    	return $this->belongsTo('App\User',"id_customer","id");
    }

    public function sender() {
    	return $this->belongsTo('App\User',"created_by","id");
    }
}
