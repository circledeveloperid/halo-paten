<?php

namespace App\Model\Master;

use Illuminate\Database\Eloquent\Model;

class kelas_master extends Model
{
    protected $table = "kelas_master";
    protected $primaryKey = "id_kelas";
    public $timestamps = false;

    public function data_kelas() {
    	return $this->hasMany('App\Model\Master\data_kelas',"id_kelas","id_kelas");
    }
}
