<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/*Untuk custom pagination*/
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
/*Untuk custom pagination*/

/* Untuk decrypt/encrypt + Custom Error */
use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Encryption\DecryptException;
/* Untuk decrypt/encrypt + Custom Error */

/* Untuk Manajemen Akun */
use Illuminate\Support\Facades\Auth;
use App\User;
use App\VarGlobal;
use Illuminate\Http\Request;
/* Untuk Manajemen Akun */

class FungsiGlobal extends Model
{
    public static function customPaginate($collection, $perPage, $pageName = 'page', $fragment = null) {
	    $currentPage = \Illuminate\Pagination\LengthAwarePaginator::resolveCurrentPage($pageName);
	    $currentPageItems = $collection->slice(($currentPage - 1) * $perPage, $perPage);
	    parse_str(request()->getQueryString(), $query);
	    unset($query[$pageName]);
	    $paginator = new \Illuminate\Pagination\LengthAwarePaginator(
	        $currentPageItems,
	        $collection->count(),
	        $perPage,
	        $currentPage,
	        [
	            'pageName' => $pageName,
	            'path' => \Illuminate\Pagination\LengthAwarePaginator::resolveCurrentPath(),
	            'query' => $query,
	            'fragment' => $fragment
	        ]
	    );

	    return $paginator;
	}

	public static function decrypt($data) {
		try {
    		$data = Crypt::decrypt($data);
    		return $data;
    	} catch (DecryptException $e) {
    		return false;
    	};
	}

	public static function encrypt($data) {
		$data = Crypt::encrypt($data);
		return $data;
	}

	public static function getToken($request) {
		return FungsiGlobal::decrypt($request->header('token'));
	}

	public static function toDBDate($stringDate) {
		return date("Y-m-d", strtotime($stringDate));
	}

	public static function set_time_zone() {
		return date_default_timezone_set("Asia/Jakarta");
	}

	public static function isNull($string) {
		$null = false;
		if(
			$string == "" ||
			$string == null ||
			$string == "#N/A" ||
			$string == "#NaN"
		)
			$null = true;
		return $null;
	}

	public static function contains($string,$sub_string) {
		if (strpos($string, $sub_string) !== false)
			return true;
		else
			return false;
	}

	public static function greeting() {
		FungsiGlobal::set_time_zone();
        $now = date("H");
        if($now>0 && $now<4)
            $time = "Dini Hari"; else
        if($now>=4 && $now<=10)
            $time = "Pagi"; else
        if($now>10 && $now<15)
            $time = "Siang"; else
        if($now>=15 && $now<=18)
            $time = "Sore"; else
            $time = "Malam";
         return $time;
	}

	public static function in_array($var,$array) {
		$res = false;
		foreach ($array as $key => $value) {
			if($value==$var) {
				$res = true;
				break;
			}
		}
		return $res;
	}

	public static function array_in_array($array,$array_two) {
		$res = false;
		foreach ($array as $key => $value) {
			foreach ($array_two as $key_two => $value_two) {
				if($value==$value_two) {
					$res = true;
					break;
				}
			}
			if($res) break;
		}
		return $res;
	}

	public static function minify_comma_string($string) {
		$array = explode(",", $string);
		$string = "";
		if(count($array)>1)
			foreach ($array as $key => $value) {
				if(count($array)-1 == $key)
					$string.=" dan ".$value;
				else
					$string.=$value.", ";
			}
		else if($count($array)==1)
			return $array[0];
		else
			return "";
		return $string;
	}

	public static function hitung_harga_merek_baru($id_permohonan) {
		// Ambil jumlah kelas
		$permohonan = \App\Model\Master\permohonan::where("id_permohonan",$id_permohonan)
			->with("data_pemohon.jenis_pemohon")
			->first();
		if(!$permohonan) return 0;
		$harga_kelas  = $permohonan->data_pemohon->jenis_pemohon->harga_merek_baru;
		$jumlah_kelas = $permohonan->jumlah_kelas;
		return (int) $harga_kelas * (int) $jumlah_kelas;
	}

	public static function cek_status_billing($id_permohonan) {
		// Ambil jumlah kelas
		$permohonan = \App\Model\Master\permohonan::where("id_permohonan",$id_permohonan)
			->with("data_pemohon.jenis_pemohon")
			->first();
		if(!$permohonan)
			return "Permohonan tidak ditemukan!";
		if($permohonan->pembayaran_dikonfirmasi && $permohonan->bukti_pembayaran==null && $permohonan->payment_id!=null)
			return "Sudah Diayar";
		if($permohonan->bukti_pembayaran==null)
			return "Belum dibayar";
		if(!$permohonan->pembayaran_dikonfirmasi)
			return "Menunggu konfirmasi pembayaran";
		else
			return "Pembayaran telah dikonfirmasi";
	}

	public static function smart_handle_auto_confirm_payment($id_pemohon=null) {
		if($id_pemohon==null)
			$payment_need_validation = \App\Model\Master\permohonan::where("payment_id","!=",null)
				->where("pembayaran_dikonfirmasi",0)
				->get();
		else
			$payment_need_validation = \App\Model\Master\permohonan::where("id_pemohon",$id_pemohon)
				->where("payment_id","!=",null)
				->where("pembayaran_dikonfirmasi",0)
				->get();
		foreach ($payment_need_validation as $key => $record) {
			$paymentController = new \App\Http\Controllers\PaymentController;
			$payed = $paymentController->payment_success($record->payment_id);
			if($payed)
				$update = \App\Model\Master\permohonan::where("id_permohonan",$record->id_permohonan)
					->update([
						"pembayaran_dikonfirmasi" => 1,
						//"id_status_permohonan" => VarGlobal::$sedang_diproses, //update baru: tidak auto diproses
					]);
		}
	}
}
