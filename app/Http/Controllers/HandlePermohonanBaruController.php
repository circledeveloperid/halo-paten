<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Master\permohonan;
use App\VarGlobal;
use App\User;
use Auth;

class HandlePermohonanBaruController extends Controller
{
    public function handle_method(Request $request, $handle=null, $param=null) {
        switch ($handle) {
            case 'konfirmasi_pembayaran':
                return $this->konfirmasi_pembayaran($param);
                break;

            case 'unkonfirmasi_pembayaran':
                return $this->unkonfirmasi_pembayaran($param);
                break;

            case 'update_status_permohonan':
                return $this->update_status_permohonan($request);
                break;

            case 'set_nomor_permohonan':
                return $this->set_nomor_permohonan($request);
                break;

            case 'upload_tanda_terima':
                return $this->upload_tanda_terima($request);
                break;

            case 'upload_surat_pernyataan':
                return $this->upload_surat_pernyataan($request);
                break;

            default:
                return [
                    "success" => false,
                    "pesan"   => "Aksi tidak dikenal!",
                ];
                break;
        }
    }

    private function konfirmasi_pembayaran($id_permohonan) {
        $update = permohonan::where("id_permohonan",$id_permohonan)->update([
            "pembayaran_dikonfirmasi" => true,
        ]);
        if($update)
            return [
                "success" => true,
                "pesan"   => "Pembayaran berhasil dikonfirmasi!",
            ];
        else
            return [
                "success" => false,
                "pesan"   => "Pembayaran sudah (atau tidak dapat) dikonfirmasi!",
            ];
    }

    private function unkonfirmasi_pembayaran($id_permohonan) {
        $update = permohonan::where("id_permohonan",$id_permohonan)->update([
            "pembayaran_dikonfirmasi" => false,
        ]);
        if($update)
            return [
                "success" => true,
                "pesan"   => "Konfirmasi pembayaran berhasil dibatalkan!",
            ];
        else
            return [
                "success" => false,
                "pesan"   => "Konfirmasi pembayaran sudah (atau tidak dapat) dibatalkan!",
            ];
    }

    private function update_status_permohonan($request) {
        $id_permohonan = $request->id_permohonan;
        $id_status = $request->id_status;
        $permohonan = permohonan::where("id_permohonan",$id_permohonan)
                        ->with("pemohon")
                        ->first();
        if(!$permohonan)
            return [
                "success" => false,
                "pesan"   => "Permohonan tidak ditemukan!",
            ];
        // Proses permohonan
        $my_user_id = Auth::user()->id;
        $msg_success = "";
        // Perlakuan khusus untuk memproses permohonan
        if((int) $id_status == VarGlobal::$sedang_diproses) {
            $proses_count = permohonan::where("id_status_permohonan",VarGlobal::$sedang_diproses)
                            ->where("processed_by",$my_user_id)
                            ->count();
            if($proses_count>0) // 1 karyawan/admin hanya boleh memproses 1 permohonan (harus diselesaikan terlebih dahulu)
                return [
                    "success" => false,
                    "pesan"   => "Silahkan selesaikan permohonan terdahulu sebelum memproses permohonan baru!",
                ];
            $msg_success = "Permohonan berhasil diambil!";
        }
        // Perlakuan khusus untuk menyelesaikan order
        if((int) $id_status == VarGlobal::$permohonan_selesai) {
            if($permohonan->nomor_permohonan==null || $permohonan->nomor_permohonan=="")
                return [
                    "success" => false,
                    "pesan"   => "Nomor permohonan belum diinput!",
                ];
            if($permohonan->tanda_terima==null || $permohonan->tanda_terima=="")
                return [
                    "success" => false,
                    "pesan"   => "File tanda terima belum diunggah!",
                ];
            $msg_success = "Permohonan berhasil diselesaikan!";
        }
        // Perlakuan khusus jika Permohonan dibatalkan >> + credit
        if((int) $id_status == VarGlobal::$permohonan_dibatalkan) {
            $free_credit = User::where("id",$permohonan->id_pemohon)->update([
                "credit" => (int) $permohonan->pemohon->credit + ($permohonan->jumlah_kelas * VarGlobal::$harga_kelas_on_credit),
            ]);
            $msg_success = "Permohonan berhasil dibatalkan!";
        }
        // Update permohonan
        $update = permohonan::where("id_permohonan",$id_permohonan)->update([
            "id_status_permohonan" => $id_status,
            "processed_by" => $my_user_id,
        ]);
        if($update)
            return [
                "success" => true,
                "pesan"   => $msg_success,
            ];
        else
            return [
                "success" => false,
                "pesan"   => "Status permohonan sudah (atau tidak dapat) diupdate!",
            ];
    }

    private function set_nomor_permohonan($request) {
        $id_permohonan = $request->id_permohonan;
        $nomor_permohonan = $request->nomor_permohonan;
         $update = permohonan::where("id_permohonan",$id_permohonan)->update([
            "nomor_permohonan" => $nomor_permohonan,
        ]);
        if($update)
            return [
                "success" => true,
                "pesan"   => "Nomor permohonan berhasil diupdate!",
            ];
        else
            return [
                "success" => false,
                "pesan"   => "Nomor permohonan sudah (atau tidak dapat) diupdate!",
            ];
    }

    private function upload_tanda_terima($request) {
        if($request->hasFile("tanda_terima")) {
            $search = permohonan::where("id_permohonan",$request->id_permohonan)->with("pemohon")->first();
            if(!$search)
                return [
                    "success" => false,
                    "pesan"   => "Permohonan tidak ditemukan!",
                ];
            $update = permohonan::where("id_permohonan",$request->id_permohonan)->update([
                "tanda_terima" => $this->save_tanda_terima($request->file("tanda_terima"),$search->pemohon),
            ]);
            if($update)
                return [
                    "success" => true,
                    "pesan"   => "Tanda terima berhasil diunggah!",
                ];
            else
                return [
                    "success" => false,
                    "pesan"   => "Tanda terima sudah (atau tidak dapat) diunggah!",
                ];
        } else
            return [
                "success" => false,
                "pesan"   => "Unhandle Request!",
            ];
    }

    private function save_tanda_terima($file,$user) {
        $tujuan_upload = 'upload/user_'.$user->id.'/permohonan_baru/'.'tanda_terima/';
        $file_name = date("YmdHis")."_".$file->getClientOriginalName();
        $file->move($tujuan_upload,$file_name);
        return $tujuan_upload.$file_name;
    }

    private function upload_surat_pernyataan($request) {
        if($request->hasFile("surat_pernyataan")) {
            $search = permohonan::where("id_permohonan",$request->id_permohonan)->with("pemohon")->first();
            if(!$search)
                return [
                    "success" => false,
                    "pesan"   => "Permohonan tidak ditemukan!",
                ];
            $update = permohonan::where("id_permohonan",$request->id_permohonan)->update([
                "surat_pernyataan" => $this->save_surat_pernyataan($request->file("surat_pernyataan"),$search->pemohon),
            ]);
            if($update)
                return [
                    "success" => true,
                    "pesan"   => "Surat_pernyataan berhasil diunggah!",
                ];
            else
                return [
                    "success" => false,
                    "pesan"   => "Surat_pernyataan sudah (atau tidak dapat) diunggah!",
                ];
        } else
            return [
                "success" => false,
                "pesan"   => "Unhandle Request!",
            ];
    }

    private function save_surat_pernyataan($file,$user) {
        $tujuan_upload = 'upload/user_'.$user->id.'/permohonan_baru/'.'surat_pernyataan/';
        $file_name = date("YmdHis")."_".$file->getClientOriginalName();
        $file->move($tujuan_upload,$file_name);
        return $tujuan_upload.$file_name;
    }
}
