<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Master\permohonan;
use App\Model\Master\negara;
use App\Model\Master\tipe_permohonan;
use App\Model\Master\tipe_merek;
use App\Model\Master\jenis_pemohon;
use App\Model\Master\kelas_master;

class DetailController extends Controller
{
    public function invoice($id_permohonan=null) {
    	if($id_permohonan==null)
    		return [
    			"success" => false,
    			"pesan"   => "Akses tidak valid!",
    		];
    	$permohonan = permohonan::where("id_permohonan",$id_permohonan)
    					->with("pemohon")
    					->with("status_permohonan")
    					->with("data_pemohon_lainnya")
    					->with("data_merek.tipe_merek")
    					->with("data_merek.tipe_permohonan")
    					->with("data_kelas.kelas_master")
    					->with("data_berkas.syarat_berkas")
    					->with("data_pemohon.jenis_pemohon")
    					->with("data_pemohon.kewarganegaraan")
    					->with("data_pemohon.kota.provinsi.negara")
    					->with("data_pemohon.kota_other.provinsi.negara")
    					->first();
    	if(!$permohonan)
    		return [
    			"success" => false,
    			"pesan"   => "Invoice tidak tersedia!",
    		];
    	else {
    		//return $permohonan;
    		$negara = negara::all();
	        $tipe_permohonan = tipe_permohonan::all();
	        $tipe_merek = tipe_merek::all();
	        $jenis_pemohon = jenis_pemohon::all();
	        $kelas = kelas_master::select("kelas")->groupBy("kelas")->get();
    		return view("page.other.invoice")
    			->with("permohonan",$permohonan)
    			->with("negara",$negara)
    			->with("tipe_permohonan",$tipe_permohonan)
    			->with("tipe_merek",$tipe_merek)
    			->with("jenis_pemohon",$jenis_pemohon)
    			->with("kelas",$kelas);
    	}
    }
}
