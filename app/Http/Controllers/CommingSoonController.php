<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CommingSoonController extends Controller
{
    public function service() {
    	return view("comming_soon.service");
    }
}
