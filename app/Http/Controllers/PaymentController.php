<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Plugins\Xendit\XenditPHPClient;
use App\Model\Master\permohonan;
use App\FungsiGlobal;
use App\VarGlobal;

class PaymentController extends Controller
{

    public function check() {
    	$XenditPHPClient = new XenditPHPClient();
    	return $XenditPHPClient->getInvoice("5de7b70b03a3cd3f45eb6771");
    	return $XenditPHPClient->getInvoice("5de7b63103a3cd3f45eb676c");
    	return $XenditPHPClient->getBalance();
    }

    public function payment_success($payment_id) {
    	$XenditPHPClient = new XenditPHPClient();
    	$access_api = $XenditPHPClient->getInvoice($payment_id);
        if($payment_id==null || $payment_id=="") return false;
    	if(!$access_api['success']) return false;
        if(!isset($access_api['data']['status'])) return false;
    	if(FungsiGlobal::in_array($access_api['data']['status'], VarGlobal::$array_dibayar))
    		return true;
    	else
    		return false;
    }

    public function bayarMerekBaru($id=null) {
    	if($id==null)
    		return [
    			"success" => false,
    			"pesan"   => "Akses tidak valid!",
    		];
    	$permohonan = permohonan::where("id_permohonan",$id)->first();
    	if(!$permohonan)
    		return [
    			"success" => false,
    			"pesan"   => "Permohonan tidak ditemukan!",
    		];
    	if($permohonan->pembayaran_dikonfirmasi)
    		return [
    			"success" => false,
    			"pesan"   => "Pembayaran telah dikonfirmasi!",
    		];
    	// Prepare data
    	$id_permohonan = $id;
    	$jumlah_pembayaran = FungsiGlobal::hitung_harga_merek_baru($id_permohonan);
    	$email_pembayar = $permohonan->pemohon->email;
    	$deskripsi = "Pembayaran Merek ".$permohonan->data_merek->merek." di HaloPaten";
    	// Generate invoice
    	$XenditPHPClient = new XenditPHPClient();
		$access_api = $XenditPHPClient->createInvoice("INV".$id_permohonan,$jumlah_pembayaran,$email_pembayar,$deskripsi);
		if(!$access_api['success'])
			return [
				"success" => false,
				"pesan"   => "Akses API ke Payment Gateway gagal!",
			];
		$payment_id  = $access_api['data']['id'];
		$invoice_url = $access_api['data']['invoice_url'];
		$expiry_date = date("Y-m-d H:i:s", strtotime($access_api['data']['expiry_date']));
		// Update record
		$update = $permohonan->where("id_permohonan",$id)->update([
			"payment_id" => $payment_id,
			"invoice_url" => $invoice_url,
			"expiry_date" => $expiry_date,
		]);
		if($update) {
			return redirect($invoice_url);
		} else
			return [
				"success" => false,
				"pesan"   => "Generate pembayaran gagal dilakukan, coba lagi nanti!",
			];
    }
}
