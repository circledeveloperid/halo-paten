<?php

namespace App\Http\Controllers\GUI;

use Illuminate\Http\Request;
use App\ClassMenu;
use Auth;
use DB; 
use App\VarGlobal;
use App\FungsiGlobal;
use App\User;
use App\Model\Master\permohonan;

class DashboardController extends \App\Http\Controllers\DefaultController
{
    public function index(Request $request)
    {
    	switch (Auth::user()->usertype) {
    		case 'karyawan':
    			$jml_customer = User::where("usertype","user")->count();
    			$jml_p_pending = permohonan::where("pembayaran_dikonfirmasi",0)->count();
    			$jml_p_proses = permohonan::whereIn("id_status_permohonan",[VarGlobal::$sedang_diproses,VarGlobal::$belum_diproses])
    									->where("pembayaran_dikonfirmasi",1)
    									->count();
    			$jml_p_selesai = permohonan::where("id_status_permohonan",VarGlobal::$permohonan_selesai)->count();
    			return view("cpanel.dashboard.karyawan")
    				->with("jml_customer",$jml_customer)
    				->with("jml_p_pending",$jml_p_pending)
    				->with("jml_p_proses",$jml_p_proses)
    				->with("jml_p_selesai",$jml_p_selesai);
    			break;

    		case 'admin':
    			$jml_karyawan = User::where("usertype","karyawan")->count();
    			$jml_customer = User::where("usertype","user")->count();
    			$jml_p_proses = permohonan::whereIn("id_status_permohonan",[VarGlobal::$sedang_diproses,VarGlobal::$belum_diproses])
                                        ->where("pembayaran_dikonfirmasi",1)
                                        ->count();
    			$jml_p_selesai = permohonan::where("id_status_permohonan",VarGlobal::$permohonan_selesai)->count();
    			return view("cpanel.dashboard.admin")
    				->with("jml_karyawan",$jml_karyawan)
    				->with("jml_customer",$jml_customer)
    				->with("jml_p_proses",$jml_p_proses)
    				->with("jml_p_selesai",$jml_p_selesai);
    			break;
    		
    		default:
    			return view("cpanel.home");
    			break;
    	}
    }
}
