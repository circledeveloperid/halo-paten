<?php

namespace App\Http\Controllers\GUI;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ErrorController extends Controller
{
    public function access_danied() {
    	return view("cpanel.errors.access_danied");
    }

    public function access_invalid() {
    	return "access_invalid";
    }
}
