<?php

namespace App\Http\Controllers\GUI;

use Illuminate\Http\Request;
use App\Model\User\User;

class UserController extends \App\Http\Controllers\DefaultController
{
    public function daftar() {
    	$data = User::with("user_role.role")->get();
    	return view("cpanel.user.list")
    		->with("data",$data);
    }

    public function tambah() {
    	return view("cpanel.user.tambah");
    }

    public function daftar_karyawan() {
    	return view("cpanel.user.karyawan");
    }

    public function tambah_karyawan() {
    	return view("cpanel.user.tambah_karyawan");
    }

    public function edit_karyawan($id=null) {
        // First validation
        if($id==null) return redirect(route('karyawan_list'));
        // Check user
        $check = User::where("id",$id)->where("usertype","karyawan")->first();
        // Editable user
        if(!$check) return redirect(route('access_invalid'));
        // Return
        return view("cpanel.user.edit_karyawan")
            ->with("karyawan",$check);
    }
}
