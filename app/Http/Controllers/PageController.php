<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Master\set_harga;
use App\VarGlobal;

class PageController extends Controller
{
    public function content_page($scope=null) {
    	switch ($scope) {
    		case 'tentang':
    			return view("page.tentang");
    			break;

    		case 'faq':
    			return view("page.faq");
    			break;

    		case 'kontak':
    			return view("page.kontak");
    			break;
    		
    		default:
                $merek_baru_umum = set_harga::where("id_set_harga",VarGlobal::$id_merek_baru_umum)->first();
                $merek_baru_umkm = set_harga::where("id_set_harga",VarGlobal::$id_merek_baru_umkm)->first();
                $merek_panjang_umum = set_harga::where("id_set_harga",VarGlobal::$id_merek_panjang_umum)->first();
                $merek_panjang_umkm = set_harga::where("id_set_harga",VarGlobal::$id_merek_panjang_umkm)->first();
                $merek_intern = set_harga::where("id_set_harga",VarGlobal::$id_merek_inter)->first();
    			return view("page.home")
                    ->with("merek_baru_umum",$merek_baru_umum)
                    ->with("merek_baru_umkm",$merek_baru_umkm)
                    ->with("merek_panjang_umum",$merek_panjang_umum)
                    ->with("merek_panjang_umkm",$merek_panjang_umkm)
                    ->with("merek_intern",$merek_intern);
    			break;
    	}
    }
}
