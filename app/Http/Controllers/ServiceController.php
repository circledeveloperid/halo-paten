<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Master\negara;
use App\Model\Master\provinsi;
use App\Model\Master\kota;
use App\Model\Master\kelas_master;
use App\Model\Master\permohonan;
use App\Model\Master\status_permohonan;
use App\Model\Master\kritik_saran;
use App\Model\Master\customer_message;
use Datatables;
use App\VarGlobal;
use App\FungsiGlobal;
use Mail;
use DB;
use Auth;

class ServiceController extends Controller
{
    public function index(Request $request, $service=null,$param=null) {
    	switch ($service) {
    		case 'getNegara':
    			return $this->get_negara();
    			break;
    		case 'getProvinsi':
    			return $this->get_provinsi($param);
    			break;
    		case 'getKota':
    			return $this->get_kota($param);
    			break;
            case 'getDataKelas':
                return $this->get_data_kelas($request);
                break;
            case 'getBillingSaya':
                return $this->get_billing_saya($param);
                break;
            case 'getPermohonanSaya':
                return $this->get_permohonan_saya($param);
                break;
            case 'getPermohonanPending':
                return $this->get_permohonan_pending();
                break;
            case 'getPermohonanMasuk':
                return $this->get_permohonan_masuk();
                break;
            case 'getPermohonanDiproses':
                return $this->get_permohonan_diproses();
                break;
            case 'getPermohonanBerhasil':
                return $this->get_permohonan_berhasil();
                break;
            case 'getPermohonanDibatalkan':
                return $this->get_permohonan_dibatalkan();
                break;
            case 'getPermohonanRevisiMasuk':
                return $this->get_permohonan_revisi_masuk();
                break;
            case 'sendKirtikSaran':
                return $this->send_kritik_saran($request);
                break;
            case 'getKritikSaran':
                return $this->get_kritik_saran();
                break;
            case 'sendQuestionToEmail':
                return $this->send_question_to_email($request);
                break;
            case 'getConversationCustomerMessage':
                return $this->get_conversation_customer_message();
                break;
    		default:
    			return $this->unknown_serive();
    			break;
    	}
    }

    private function unknown_serive() {
    	return [
    		"success" => true,
    		"pesan"   => "Service tidak dikenal!",
    	];
    }

    private function get_negara() {
    	return negara::all();
    }

    private function get_provinsi($id_negara) {
        if($id_negara==null) return [];
    	return provinsi::where("country_id",$id_negara)->get();
    }

    private function get_kota($id_provinsi) {
        if($id_provinsi==null) return [];
        return kota::where("state_id",$id_provinsi)->get();
    }

    private function get_data_kelas($request) {
        $model = kelas_master::query();
        // Filter kelas
        if($request->has("kelas"))
            $model = $model->where("kelas",$request->kelas);

        if($request->has("uraian_id"))
            $model = $model->where("deskripsi_id","LIKE","%".$request->uraian_id."%");

        if($request->has("uraian_en"))
            $model = $model->where("deskripsi_en","LIKE","%".$request->uraian_en."%");

        return Datatables::of($model)->make(true);
    }

    private function get_billing_saya($id_user) {
        if($id_user==null) return [];
        $model = permohonan::query();
        $model = $model->where("id_pemohon",$id_user);
        $model = $model->where("pembayaran_dikonfirmasi",false);
        $model = $model
                   /* ->with("data_pemohon.kota.provinsi.negara")
                    ->with("data_pemohon.kewarganegaraan")
                    ->with("data_pemohon.kota_other.provinsi.negara")
                    ->with("data_pemohon.jenis_pemohon")
                    ->with("data_merek.tipe_permohonan")*/
                    ->with("data_merek.tipe_merek");
                    /*->with("data_pemohon_lainnya")
                    ->with("data_berkas.syarat_berkas")
                    ->with("data_kelas.kelas_master")*/
        return Datatables::of($model)
                ->addColumn('harga', function($model){
                    $harga = FungsiGlobal::hitung_harga_merek_baru($model->id_permohonan);
                    return "Rp".number_format($harga).",-";
                })
                ->addColumn('status_billing', function($model){
                    $status = FungsiGlobal::cek_status_billing($model->id_permohonan);
                    return $status;
                })
                ->addColumn('need_generate_payment', function($model){
                    $expiry_date = $model->expiry_date;
                    if($expiry_date==null || $expiry_date=="")
                        return true;
                    else {
                        $expiry_date = strtotime(date($model->expiry_date));
                        $now = strtotime(date("Y-m-d H:i:s"));
                        if($now>$expiry_date)
                            return true;
                        else
                            return false;
                    }
                })
                ->make(true);
    }

    private function get_permohonan_saya($id_user) {
        if($id_user==null) return [];
        $model = permohonan::query();
        $model = $model->where("id_pemohon",$id_user);
        $model = $this->generate_detail_permohonan($model);
        return Datatables::of($model)
                ->addColumn('harga', function($model){
                    $harga = FungsiGlobal::hitung_harga_merek_baru($model->id_permohonan);
                    return "Rp".number_format($harga).",-";
                })
                ->addColumn('status_billing', function($model){
                    $status = FungsiGlobal::cek_status_billing($model->id_permohonan);
                    return $status;
                })
                ->make(true);
    }

    private function get_permohonan_dibatalkan() {
        $model = permohonan::query();
        $model = $this->generate_detail_permohonan($model);
        $model = $model->where("pembayaran_dikonfirmasi",true);
        $model = $model->where("id_status_permohonan",VarGlobal::$permohonan_dibatalkan);
        $status_permohonan = status_permohonan::all();
        return Datatables::of($model)
                ->addColumn('harga', function($model){
                    $harga = FungsiGlobal::hitung_harga_merek_baru($model->id_permohonan);
                    return "Rp".number_format($harga).",-";
                })
                ->addColumn('status_billing', function($model){
                    $status = FungsiGlobal::cek_status_billing($model->id_permohonan);
                    return $status;
                })
                ->addColumn('kelas_dipilih', function($model){
                    $kelas_dipilih = [];
                    foreach ($model->data_kelas as $key => $value) {
                        $kelas = $value->kelas_master->kelas;
                        if(!FungsiGlobal::in_array($kelas, $kelas_dipilih))
                            array_push($kelas_dipilih, $kelas);
                    }
                    return $kelas_dipilih;
                })
                ->make(true);
    } 

    private function get_permohonan_berhasil() {
        $model = permohonan::query();
        $model = $this->generate_detail_permohonan($model);
        $model = $model->where("pembayaran_dikonfirmasi",true);
        $model = $model->where("id_status_permohonan",VarGlobal::$permohonan_selesai);
        return Datatables::of($model)
                ->addColumn('harga', function($model){
                    $harga = FungsiGlobal::hitung_harga_merek_baru($model->id_permohonan);
                    return "Rp".number_format($harga).",-";
                })
                ->addColumn('status_billing', function($model){
                    $status = FungsiGlobal::cek_status_billing($model->id_permohonan);
                    return $status;
                })
                ->addColumn('kelas_dipilih', function($model){
                    $kelas_dipilih = [];
                    foreach ($model->data_kelas as $key => $value) {
                        $kelas = $value->kelas_master->kelas;
                        if(!FungsiGlobal::in_array($kelas, $kelas_dipilih))
                            array_push($kelas_dipilih, $kelas);
                    }
                    return $kelas_dipilih;
                })
                ->make(true);
    }

    private function get_permohonan_diproses() {
        $model = permohonan::query();
        $model = $this->generate_detail_permohonan($model);
        $model = $model->where("pembayaran_dikonfirmasi",true);
        $model = $model->where("is_revisi",false);
        $model = $model->where("processed_by",Auth::user()->id);
        $model = $model->whereIn("id_status_permohonan",[VarGlobal::$sedang_diproses]);
        return Datatables::of($model)
                ->addColumn('harga', function($model){
                    $harga = FungsiGlobal::hitung_harga_merek_baru($model->id_permohonan);
                    return "Rp".number_format($harga).",-";
                })
                ->addColumn('status_billing', function($model){
                    $status = FungsiGlobal::cek_status_billing($model->id_permohonan);
                    return $status;
                })
                ->addColumn('kelas_dipilih', function($model){
                    $kelas_dipilih = [];
                    foreach ($model->data_kelas as $key => $value) {
                        $kelas = $value->kelas_master->kelas;
                        if(!FungsiGlobal::in_array($kelas, $kelas_dipilih))
                            array_push($kelas_dipilih, $kelas);
                    }
                    return $kelas_dipilih;
                })
                ->make(true);
    } 

    private function get_permohonan_masuk() {
        $model = permohonan::query();
        $model = $this->generate_detail_permohonan($model);
        $model = $model->where("pembayaran_dikonfirmasi",true);
        $model = $model->where("is_revisi",false);
        $model = $model->whereIn("id_status_permohonan",[VarGlobal::$belum_diproses]);
        return Datatables::of($model)
                ->addColumn('harga', function($model){
                    $harga = FungsiGlobal::hitung_harga_merek_baru($model->id_permohonan);
                    return "Rp".number_format($harga).",-";
                })
                ->addColumn('status_billing', function($model){
                    $status = FungsiGlobal::cek_status_billing($model->id_permohonan);
                    return $status;
                })
                ->addColumn('kelas_dipilih', function($model){
                    $kelas_dipilih = [];
                    foreach ($model->data_kelas as $key => $value) {
                        $kelas = $value->kelas_master->kelas;
                        if(!FungsiGlobal::in_array($kelas, $kelas_dipilih))
                            array_push($kelas_dipilih, $kelas);
                    }
                    return $kelas_dipilih;
                })
                ->make(true);
    }

    private function get_permohonan_revisi_masuk() {
        $model = permohonan::query();
        $model = $this->generate_detail_permohonan($model);
        $model = $model->where("pembayaran_dikonfirmasi",true);
        $model = $model->where("is_revisi",true);
        $model = $model->whereIn("id_status_permohonan",[VarGlobal::$belum_diproses, VarGlobal::$sedang_diproses]);
        return Datatables::of($model)
                ->addColumn('harga', function($model){
                    $harga = FungsiGlobal::hitung_harga_merek_baru($model->id_permohonan);
                    return "Rp".number_format($harga).",-";
                })
                ->addColumn('status_billing', function($model){
                    $status = FungsiGlobal::cek_status_billing($model->id_permohonan);
                    return $status;
                })
                ->addColumn('kelas_dipilih', function($model){
                    $kelas_dipilih = [];
                    foreach ($model->data_kelas as $key => $value) {
                        $kelas = $value->kelas_master->kelas;
                        if(!FungsiGlobal::in_array($kelas, $kelas_dipilih))
                            array_push($kelas_dipilih, $kelas);
                    }
                    return $kelas_dipilih;
                })
                ->make(true);
    }

    private function get_permohonan_pending() {
        $model = permohonan::query();
        $model = $model->where("pembayaran_dikonfirmasi",false);
        $model = $this->generate_detail_permohonan($model);
        return Datatables::of($model)
                ->addColumn('harga', function($model){
                    $harga = FungsiGlobal::hitung_harga_merek_baru($model->id_permohonan);
                    return "Rp".number_format($harga).",-";
                })
                ->addColumn('status_billing', function($model){
                    $status = FungsiGlobal::cek_status_billing($model->id_permohonan);
                    return $status;
                })
                ->addColumn('kelas_dipilih', function($model){
                    $kelas_dipilih = [];
                    foreach ($model->data_kelas as $key => $value) {
                        $kelas = $value->kelas_master->kelas;
                        if(!FungsiGlobal::in_array($kelas, $kelas_dipilih))
                            array_push($kelas_dipilih, $kelas);
                    }
                    return $kelas_dipilih;
                })
                ->make(true);
    }

    private function generate_detail_permohonan($model) {
        return $model
                    ->with("data_pemohon.kota.provinsi.negara")
                    ->with("data_pemohon.kewarganegaraan")
                    ->with("data_pemohon.kota_other.provinsi.negara")
                    ->with("data_pemohon.jenis_pemohon")
                    ->with("data_merek.tipe_permohonan")
                    ->with("data_merek.tipe_merek")
                    ->with("data_pemohon_lainnya")
                    ->with("data_berkas.syarat_berkas")
                    ->with("data_kelas.kelas_master")
                    ->with("status_permohonan");
    }

    private function send_kritik_saran($request) {
        $tambah = new kritik_saran;
            $tambah->name = $request->nama;
            $tambah->email = $request->email;
            $tambah->deskripsi = $request->pernyataan;
        if($tambah->save())
            return [
                "success" => true,
                "pesan"   => "Pertanyaan/pernyataan berhasil dikirim!",
            ];
        else
            return [
                "success" => true,
                "pesan"   => "Pertanyaan/pernyataan gagal dikirim!",
            ];
    }

    private function get_kritik_saran() {
        $model = kritik_saran::query();
        return Datatables::of($model)->make(true);
    }

    private function send_question_to_email($request) {
        try{
            Mail::send('email.email_to_department', [
                'nama_customer' => $request->sender_name,
                'email_customer' => $request->sender_email,
                'ponsel_customer' => $request->sender_phone,
                'sender_question' => $request->sender_question
            ], function ($message) use ($request)
            {
                $message->subject("Halo Paten: Question to Department from ".$request->sender_name);
                $message->from($request->sender_email, $request->sender_name);
                $message->to($request->receiver_email);
            });
            return [
                "success" => true,
                "pesan"   => "Email berhasil dikirim ke department yang bersangkutan!",
            ];
        }
        catch (Exception $e){
            return [
                "success" => false,
                "pesan"   => $e->getMessage(),
            ];
        }
    }

    private function get_conversation_customer_message() {
        $customers = customer_message::select("id_customer AS customer_id")
                        ->distinct()
                        ->get();
        foreach ($customers as $key => $value) {
            $recent_message = customer_message::where("id_customer",$value->customer_id)
                                ->orderBy("id_chat","DESC")
                                ->with("customer")
                                ->first();
            $customers[$key]->id_chat = $recent_message->id_chat;
            $customers[$key]->id_customer = $recent_message->id_customer;
            $customers[$key]->is_answer = $recent_message->is_answer;
            $customers[$key]->message = strlen($recent_message->message) > 100 ? substr($recent_message->message,0,100)."..." : $recent_message->message;
            $customers[$key]->created_at = $recent_message->created_at;
            $customers[$key]->updated_at = $recent_message->updated_at;
            $customers[$key]->has_read = $recent_message->has_read;
            $customers[$key]->created_by = $recent_message->created_by;
            $customers[$key]->customer = $recent_message->customer;
        }
        return Datatables::of($customers)->make(true);

    }
}
