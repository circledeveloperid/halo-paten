<?php

namespace App\Http\Controllers\API\Service;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ClassAkun;
use App\User;
use Datatables;
use DB;

class UserController extends Controller
{
	public function getUser(Datatables $datatables) {
    	 $model = User::query();
    	 return Datatables::of($model)->make(true);
    }

    public function getCustomer(Datatables $datatables) {
    	 $model = User::query()->where("usertype","user");
    	 return Datatables::of($model)->make(true);
    }

    public function getKaryawan(Datatables $datatables) {
    	 $model = User::query()->whereIn("usertype",["pegawai","karyawan"]);
    	 return Datatables::of($model)->make(true);
    }
}
