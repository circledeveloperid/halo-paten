<?php

namespace App\Http\Controllers\API\Service;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Master\set_harga;
use App\Model\Master\jenis_pemohon;

class HargaController extends Controller
{
    public function setHarga(Request $request, $id_set_harga=0) {
    	$search = set_harga::where("id_set_harga",$id_set_harga)->first();
    	if(!$search)
    		return [
    			"success" => false,
    			"pesan"   => "Pengaturan tidak ditemukan!",
    		];
    	$prev_price = $search->harga;
    	if($search->jenis_pemohon!=null)
    		$success_update = jenis_pemohon::where("id_jenis_pemohon",$search->id_jenis_ref)->update([
    			"$search->field_target_ref" => $request->harga,
    		]);
        else
            $success_update = true;

        if($success_update) {
                $update_harga = set_harga::where("id_set_harga",$id_set_harga)->update([
                    "harga" => $request->harga,
                ]);
                if(!$update_harga)
                    return [
                        "success" => false,
                        "pesan"   => "Harga sudah (atau tidak dapat) diupdate!",
                    ];
            } else
                return [
                    "success" => false,
                    "pesan"   => "Harga sudah (atau tidak dapat) diupdate!",
                ];
                
    	return [
    		"success" => true,
    		"pesan"   => "Harga bergasil diupdate!",
    	];
    }
}
