<?php

namespace App\Http\Controllers\API\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Other\Landing_Set;

class LandingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $first = Landing_Set::orderBy("id_landing")->first();
        $update = Landing_Set::where("id_landing",$first->id_landing)->update([
            "about" => $request->about,
            "blackquote_content" => $request->blackquote_content,
            "blackquote_author" => $request->blackquote_author,
            "blackquote_title" => $request->blackquote_title,
            "contact" => $request->contact,
            "email" => $request->email,
        ]);
        if($update)
            return [
                "success" => true,
                "pesan"   => "Pengaturan berhasil disimpan!",
            ];
        else
            return [
                "success" => false,
                "pesan"   => "Pengaturan gagal disimpan!",
            ];
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
