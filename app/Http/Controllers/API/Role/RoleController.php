<?php

namespace App\Http\Controllers\API\Role;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Role\Role;
use App\Model\Role\UserRole;
use App\FungsiGlobal;
use App\VarGlobal;
use App\ClassAkun;
use App\User;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return $this->show(FungsiGlobal::getToken($request));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(
            $request->has("id_user") &&
            $request->has("roles")
        ) {
            $roles = $request->roles;
            $id_user = $request->id_user;
            if($this->isUserRegistered($id_user)) {
                $hapus = UserRole::where("id_user",$request->id_user)->delete();
                $is_admin = false;
                $is_karyawan = false;
                foreach ($roles as $key => $value) {
                    $tambah = new UserRole;
                        $tambah->id_user = $id_user;
                        $tambah->id_role = $value;
                    $tambah->save();
                    if(FungsiGlobal::in_array((int) $value, VarGlobal::$array_admin)) $is_admin = true;
                    if(FungsiGlobal::in_array((int) $value, VarGlobal::$array_karyawan)) $is_karyawan = true;
                }
                // Set new usertype
                $usertype = "user";
                if($is_admin) $usertype = "admin"; else
                if($is_karyawan) $usertype = "karyawan";
                // Update new usertype
                $update = User::where("id",$id_user)->where("usertype","!=",$usertype)->update([
                    "usertype" => $usertype
                ]);
                return [
                    "success" => true,
                    "pesan"   => "Role pengguna berhasil ditetapkan!",
                ];
            } else
                return [
                    "success" => false,
                    "pesan"   => "Pengguna tidak terdaftar!",
                ];
        } else
            return [
                "success" => false,
                "pesan"   => "Perubahan role pengguna tidak dapat diterapkan",
            ];
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id=0)
    {
        if($this->isUserRegistered($id)) {
            return [
                "success" => true,
                "pesan"   => "Role pengguna berhasil diambil!",
                "data"    => (object) [
                    "available_role" => $this->getAvailableRole($id),
                    "user_role"      => $this->getUserRole($id),
                ],
            ];
        } else
            return [
                "success" => false,
                "pesan"   => "Pengguna tidak terdaftar!",
            ];
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getUserRole($id_user) {
        return Role::whereHas("user_role", function($query) use ($id_user) {
                    $query->where("id_user",$id_user);
                })
                ->get();
    }

    public function getAvailableRole($id_user) {
        return Role::whereDoesntHave("user_role", function($query) use ($id_user) {
                    $query->where("id_user",$id_user);
                })
                ->get();
    }

    public function isUserRegistered($id_user) {
        $cek = ClassAkun::select("id")->where("id",$id_user)->first();
        if($cek) {
            return true;
        } else {
            // register pengguna
            $get = User::where("id",$id_user)->first();
            if($get) {
                // tambahkan pengguna
                $tambah = new ClassAkun;
                    $tambah->id = $id_user;
                $tambah->save();
                return true;
            } else
                return false;
        };
    }
}
