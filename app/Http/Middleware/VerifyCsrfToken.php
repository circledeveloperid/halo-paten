<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        '/logout',
        '/service/*',
        '/user/permohonan-user/*',
        '/user/permohonan-user',
        '/user/permohonan-user-revisi/*',
        '/user/permohonan-user-revisi',
        'api/*',
        '/logout',
    ];
}
