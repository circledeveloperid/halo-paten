<?php

namespace App\Http\Middleware;

use Closure;
use App\FungsiGlobal;
use App\ClassRole;
use Illuminate\Support\Facades\Route;
use App\Model\Menu\MenuChild;
use Auth;

class AuthMenu
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //return $next($request);
        $canAccess = false;
        // get current RuleName
        $routeName = Route::currentRouteName();
        // get rule can access
        $ruleCanAccess = MenuChild::where("route_name",$routeName)->first();
        if($ruleCanAccess==null)
            $canAccess = true;
        else {
            // get idUser
            $idUser = Auth::user()->id;
            // get myRole
            $role   = new ClassRole;
            $myRole = $role->getMyRole($idUser);
            // validasi Role
            foreach ($ruleCanAccess->role_menu as $keyOne => $valueOne) {
                foreach ($myRole as $keyTwo => $valueTwo) {
                    if($valueOne->id_role==$valueTwo->id_role) {
                        $canAccess = true; break;
                    }
                }
                if($canAccess==true) break;
            }
        };
        if($canAccess==true)
            return $next($request);
        else
            return redirect(Route("access_danied"));
    }
}
