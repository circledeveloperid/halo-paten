<script type="text/javascript">
  $syarat_upload_berkas = [
    @foreach($jenis_pemohon as $val)
      {
        "id_jenis_pemohon" : "{{$val->id_jenis_pemohon}}",
        "jenis_pemohon" : "{{$val->jenis_pemohon}}",
        "syarat_berkas" : [
            @foreach($val->syarat_berkas as $syarat)
            {
              "id_syarat_berkas" : "{{$syarat->id_syarat_berkas}}",
              "jenis_berkas" : "{{$syarat->jenis_berkas}}",
            },
            @endforeach
        ],
      },
    @endforeach
  ];

  function handle_syarat_berkas($jenis_pemohon) {
    $has_complete = true;
    for($i=0;$i<$syarat_upload_berkas.length;$i++) {
      if($syarat_upload_berkas[$i].id_jenis_pemohon!=$jenis_pemohon) continue;
      $syarat_berkas = $syarat_upload_berkas[$i].syarat_berkas;
      for($x=0;$x<$syarat_berkas.length;$x++) {
        $filled = $("#syarat_berkas_"+$syarat_berkas[$x].id_syarat_berkas).val();
        if($filled=="" || $filled.trim().length==0) {
          showToast("warning","Berkas \""+$syarat_berkas[$x].jenis_berkas+"\" belum dipilih!");
          $has_complete = false;
          break;
        }
      }
    }
    return $has_complete;
  }

  function handle_opsi_pemilihan_kelas($unknown_class_description_tmp) {
    $allow_continue = true;
    $unknown_class_tmp = false;
    if($("#unknown_class").is(":checked")) $unknown_class_tmp = true;
    if($unknown_class_tmp) {
      if(
        $unknown_class_description_tmp=="" ||
        $unknown_class_description_tmp==null ||
        $unknown_class_description_tmp==NaN
        ) {
        showToast("warning","Deskripsi kelas belum diinput!");
        $allow_continue = false;
      }
    } else {
      $jumlah_kelas = $("#jumlah_kelas").val();
      if($jumlah_kelas<1) {
        showToast("warning","Kelas belum dipilih!");
        $allow_continue = false;
      }
    }
    return $allow_continue;
  }

  function append_syarat_berkas($jenis_pemohon) {
    for($i=0;$i<$syarat_upload_berkas.length;$i++) {
      if($syarat_upload_berkas[$i].id_jenis_pemohon!=$jenis_pemohon) continue;
      $syarat_berkas = $syarat_upload_berkas[$i].syarat_berkas;
      for($x=0;$x<$syarat_berkas.length;$x++) {
        $id_syarat_berkas = $syarat_berkas[$x].id_syarat_berkas
        $filled = $("#syarat_berkas_"+$id_syarat_berkas).prop('files')[0];
        $form_data.append("syarat_berkas["+$id_syarat_berkas+"]",$filled);
      }
    }
    console.log("Praprocess 1");
    return true;
  }

  function append_pemohon_tambahan() {
    $data = $("#pemohon_tambahan").DataTable().rows().data();
    for($i=0;$i<$data.length;$i++) {
      $no_ktp_tmp = $data[$i][0];
      $nama_pemohon_tmp = $data[$i][1];
      $form_data.append("pemohon_tambahan["+$no_ktp_tmp+"]",$nama_pemohon_tmp);
    }
    console.log("Praprocess 2");
    return true;
  }

  function append_pilihan_kelas() {
    for($i=0;$i<selectedClass.length;$i++) {
      $form_data.append("pilihan_kelas["+$i+"]",selectedClass[$i].id_kelas);
    }
    console.log("Praprocess 3");
    return true;
  }

  var $form_data = new FormData();

  function submit_merek_baru() {
    // Data pemohon
    $nomor_ktp = $("#nomor_ktp").val();
    $nama_pemohon = $("#nama_pemohon").val(); // require
    $kewarganegaraan = $("#kewarganegaraan").val(); // require
    $jenis_pemohon = $("input[name=jenis_pemohon]:checked").val(); //required
    $negara = $("#negara").val() //required
    $provinsi = $("#provinsi").val() //required
    $kota = $("#kota").val() //required
    $alamat = $("#alamat").val() //required
    $kode_pos = $("#kode_pos").val()
    $nomor_hp = $("#nomor_hp").val() //required
    $whatsapp = $("#whatsapp").val() //required
    $email = $("#email").val() //required
    // Alamat pemohon tambahan
    $negara_other = $("#negara_other").val()
    $provinsi_other = $("#provinsi_other").val()
    $kota_other = $("#kota_other").val()
    $alamat_other = $("#alamat_other").val()
    $kode_pos_other = $("#kode_pos_other").val()
    $nomor_hp_other = $("#nomor_hp_other").val()
    $email_other = $("#email_other").val()
    // Data Merek
    $tipe_permohonan = $("#tipe_permohonan").val(); //required
    $tipe_merek = $("#tipe_merek").val(); //required
    $label_merek = $("#label_merek").val(); //required
    $nama_merek = $("#nama_merek").val(); //required
    $unsur_warna = $("#unsur_warna").val(); //required
    // Pilihan kelas
    $pilihan_kelas = selectedClass;
    $jumlah_kelas = $("#jumlah_kelas").val();
    // Unknown class
    $unknown_class = $("#unknown_class_description").val();
    // Term and condition
    $term_condition = "#term_condition";

    //=== VALIDASI ===//
    //if(!handle_require($nomor_ktp,"Nomor KTP")) return;
    if(!handle_require($nama_pemohon,"Nama pemohon")) return;
    if(!handle_select($kewarganegaraan,"Kewarganegaraan")) return;
    if(!handle_select($jenis_pemohon,"Jenis pemohon")) return;
    if(!handle_select($negara,"Negara")) return;
    if(!handle_select($provinsi,"Provinsi")) return;
    if(!handle_select($kota,"Kota")) return;
    if(!handle_require($alamat,"Alamat")) return;
    if(!handle_require($nomor_hp,"Nomor HP/Telp")) return;
    if(!handle_require($whatsapp,"Nomor WhatsApp")) return;
    if(!handle_require($email,"Alamat email")) return;
    if(!handle_select($tipe_permohonan,"Tipe Permohononan")) return;
    if(!handle_select($tipe_merek,"Tipe Merek")) return;
    if(!handle_require($label_merek,"Logo merek")) return;
    if(!handle_require($nama_merek,"Nama merek")) return;
    if(!handle_require($unsur_warna,"Unsur warna")) return;
    // Valiasi jumlah kelas // digabung dengan method handle_opsi_pemilihan_kelas
    /*if($pilihan_kelas.length<1) {
      showToast("warning","Kelas belum dipilih!");
      return;
    }*/

    // Unknown class
    if(!handle_opsi_pemilihan_kelas($unknown_class)) return;
    if($("#unknown_class").is(":checked")) $jumlah_kelas = 1;

    // Validasi file berkas
    if(!handle_syarat_berkas($jenis_pemohon)) return;

    // Tranform image file
    $label_merek = $("#label_merek").prop('files')[0];

    // Term and condition
    if(!handle_checked($term_condition,"Term and condition")) return;

    // Preparation data
    $form_data = new FormData(); // reset
    $form_data.append('no_ktp',$nomor_ktp);
    $form_data.append('nama_pemohon',$nama_pemohon);
    $form_data.append('kewarganegaraan',$kewarganegaraan);
    $form_data.append('jenis_pemohon',$jenis_pemohon);
    $form_data.append('kota',$kota);
    $form_data.append('alamat',$alamat);
    $form_data.append('kode_pos',$kode_pos);
    $form_data.append('nomor_hp',$nomor_hp);
    $form_data.append('whatsapp',$whatsapp);
    $form_data.append('email',$email);
    $form_data.append('kota_other',$kota_other);
    $form_data.append('alamat_other',$alamat_other);
    $form_data.append('kode_pos_other',$kode_pos_other);
    $form_data.append('nomor_hp_other',$nomor_hp_other);
    $form_data.append('email_other',$email_other);
    $form_data.append('tipe_permohonan',$tipe_permohonan);
    $form_data.append('tipe_merek',$tipe_merek);
    $form_data.append('nama_merek',$nama_merek);
    $form_data.append('label_merek',$label_merek);
    $form_data.append('unsur_warna',$unsur_warna);
    $form_data.append('jumlah_kelas',$jumlah_kelas);
    $form_data.append('unknown_class',$unknown_class);
    $form_data.append('is_revisi',$is_revisi);

    $.when(append_syarat_berkas($jenis_pemohon)).then(function(){
      $.when(append_pemohon_tambahan()).then(function() {
        $.when(append_pilihan_kelas()).then(function(){
          console.log("Process");
          // Submit data
          @if(Auth::guest())
          redirect("{{route('login')}}");
          @else
          showLoading();
          $.ajax({
            url    : $submit_server,
            method : "POST",
            cache  : false,
            contentType: false,
            processData: false,
            headers: {
              "token"    : "{{Crypt::encrypt(auth::user()->id)}}",
            },
            data   : $form_data,
            success: function(res) {
              hideLoading();
              if(res.success) {
                showToast("success",res.pesan);
                redirect($success_submit_url,3000);
              } else
                showToast("error",res.pesan);
            },
            error  : function() {
              hideLoading();
              showToast("error","Tidak dapat terhubung ke server!");
            }
          })
          @endif
        })
      })
    })
  }
</script>