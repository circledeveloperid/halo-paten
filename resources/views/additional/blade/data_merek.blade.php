<div class="modal fade" id="modal_data_merek">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
		<h6 class="modal-title bold">Kelas Barang/Jasa</h6>
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
      </div>
      <div class="modal-body">
        <!-- Bagian Filter -->
        <form id="formTambahKelas" method="post" class="form">
			<div class="form-group row">
				<label class="col-md-4 padding-left-30">Kelas</label>
				<div class="col-md-8">
					<select class="form-control m-input" id="kelas_merek" name="classOption">
						<option value="">--Semua Kelas--</option>
						@foreach($kelas as $val)
						<option value="{{$val->kelas}}">{{$val->kelas}}</option>
						@endforeach
					</select>
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-4 padding-left-30">Uraian
					Barang/Jasa (ID)</label>
				<div class="col-sm-8">
					<textarea class="form-control" placeholder="Uraian Barang/Jasa" id="uraian_id" maxlength="4000"></textarea>
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-4 padding-left-30">Uraian
					Barang/Jasa (EN)</label>
				<div class="col-sm-8">
					<textarea class="form-control" placeholder="Uraian Barang/Jasa (English)" id="uraian_en" maxlength="4000"></textarea>
				</div>
			</div>
			<div class="form-group row">
				<div class="col-sm-4"></div>
				<div class="col-sm-8">
					<button type="button" class="btn btn-primary" onclick="filter_tabel()">
						<i class="fa fa-search"></i> Cari
					</button>
				</div>
			</div>
			<div class="row" style="padding-left:10px;padding-right:10px">
				<div class="col-sm-12">
					<table class="table table-striped table-bordered table-responsive" id="tabel_master_kelas">
						<thead>
							<tr>
								<th>
									<input type="checkbox" name="" id="select_all">
								</th>
								<th>Kelas</th>
								<th>Uraian Barang/Jasa (ID)</th>
								<th>Uraian Barang/Jasa (EN)</th>
							</tr>
						</thead>
						<tbody style="text-align:left;"></tbody>
					</table>
				</div>
			</div>
		</form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">
        	<i class="fa fa-times"></i> Batal
        </button>
        <button type="button" class="btn btn-primary" id="btnPilihKelas">
        	<i class="fa fa-check"></i> Pilih
        </button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript">
	$has_initialize_table = false;

	$(document).ready(function(){
		//$('.modal').modal("show");
		//initialize_table();
		initialize_jumlah_kelas();
	})

	function initialize_jumlah_kelas() {
		$("#jumlah_kelas").val(selectedClass.length);
	}

	function show_modal_data_merek() {
		$('#modal_data_merek').modal("show");
		if($has_initialize_table)
			filter_tabel();
		else {
			initialize_table();
			$has_initialize_table = true;
		}
	}

	function initialize_table() {
		$tabel_master_kelas = $("#tabel_master_kelas").DataTable({
	        "searching": false,
	        "ajax": {
                data: function (d) {
                	d.kelas = $("#kelas_merek").val();
                	d.uraian_id = $("#uraian_id").val();
                	d.uraian_en = $("#uraian_en").val();
                },
                type: 'POST',
                url: "{{Route('service')}}/getDataKelas"
            },
	        "processing": true,
	        "serverSide": true,
	        "columns": [
	            { "data": "id_kelas", render: function (data, type, row, meta) {
	            	if($.inArray(data, selected_id_kelas()) !== -1 )
	                	return '<input type="checkbox" class="selected_record" value="'+data+'" checked>';
	                else
	                	return '<input type="checkbox" class="selected_record" value="'+data+'">';
	            }, "searchable": false, "orderable": false},
	            { "data": "kelas" },
	            { "data": "deskripsi_id" },
	            { "data": "deskripsi_en" },
	        ],
	        "order": [[1, 'asc']]
		});
	}

	function filter_tabel() {
		$("#tabel_master_kelas").DataTable().ajax.reload();
	}

	function selected_id_kelas() {
		$res = [];
		for($i=0;$i<selectedClass.length;$i++) {
			$res.push(selectedClass[$i].id_kelas);
		}
		return $res;
	}

    var selectedClass = [];

	$('#select_all').on('click', function(){
      var rows = $tabel_master_kelas.rows({ 'search': 'applied' }).nodes();
      $('input.selected_record', rows).prop('checked', this.checked);
      $('input.selected_record', rows).change();
    });

    $('#tabel_master_kelas tbody').on('change', 'input.selected_record', function(){
      // Handle icon
      if(!this.checked){
         var el = $('#select_all').get(0);
         if(el && el.checked && ('indeterminate' in el)){
            el.indeterminate = true;
         }
      }
      // Handle data
	  var checked = this.checked;
	  var idx = $(this).attr('value');
	  if(checked) {
		var x = new Object();
		x.id_kelas = idx;
		x.kelas = $(this).parent().parent().find('td:eq(1)').text();
		x.deskripsi_id = $(this).parent().parent().find('td:eq(2)').text();
		x.deskripsi_en = $(this).parent().parent().find('td:eq(3)').text();
		selectedClass.push(x);
	  } else {
		var i;
		for (i = 0; i < selectedClass.length; i++) {
			if(selectedClass[i].id_kelas == idx) {
				selectedClass.splice(i, 1);
				break;
			}
		}
	  }
	  $(this).toggleClass('selected');
	});

    $('#btnPilihKelas').click(function () {
    	//console.log(selectedClass);
    	$("#tabel_kelas_dipilih").DataTable().clear().draw();
        for (var i = 0; i < selectedClass.length; i++) {
    		btn_hapus = "<button class='hapus btn btn-sm btn-danger' onclick='hapus_selected_class("+selectedClass[i].id_kelas+")'>Hapus</button>";
    		$("#tabel_kelas_dipilih").DataTable().row.add(
		        [selectedClass[i].kelas,selectedClass[i].deskripsi_id,btn_hapus],
		    ).draw();
        }
        update_jumlah_kelas();
        $("#modal_data_merek").modal('hide');
    });

    $('#tabel_kelas_dipilih').on('click', 'button.hapus', function () {
	    $("#tabel_kelas_dipilih").DataTable()
	        .row($(this).parents('tr'))
	        .remove()
	        .draw();
	});

	function update_jumlah_kelas() {
		var arrClass = [];
        for (var i = 0; i < selectedClass.length; i++) {
        	if(arrClass.indexOf(selectedClass[i].kelas) === -1) {
        		arrClass.push(selectedClass[i].kelas);
        	}
        }
        $('#jumlah_kelas').val(arrClass.length);
        $('#span_jml_kelas').html(arrClass.length);
	}

	function hapus_selected_class($id_kelas) {
		for (i = 0; i < selectedClass.length; i++) {
			if(selectedClass[i].id_kelas == $id_kelas) {
				selectedClass.splice(i, 1);
				break;
			}
		}
		update_jumlah_kelas();
		console.log(selectedClass);
	}
</script>