@extends("layouts.template")

@section("content")
<div class="row">
    <div class="col-md-12 block_tentang_kami" style="background:url('{{asset('konten/brand-story-new.jpg')}}');text-align:center;padding:100px;background-size:100% 100%;background-repeat:no-repeat;height:500px">
        <div style="font-size:25px; text-align: justify; padding:50px; background: rgba(10, 10, 10, 0.8);color:white;font-family:'Arial';line-height:1.3em" class="tentang_kami_txt">
            Dalam sebuah usaha, merek bisa menjadi aset yang paling berharga apabila sudah dikenal luas oleh konsumen. Sebuah bisnis dapat tumbuh dan menjadi besar karena memanfaatkan kekuatan merek.
        </div>
    </div>
</div>

<br>

<div class="row">
	<div class="col-md-12">
		<br>
		<h4 class="center black bold">Keuntungan Memiliki Merek</h4>
	</div>
</div>

<br>

<div class="row">
	<div class="col-md-6">
		<img src="{{asset('konten/1.jpg')}}" class="shadow-box img-box-shadow">
	</div>
	<div class="col-md-6">
		<img src="{{asset('konten/2.jpg')}}" class="shadow-box img-box-shadow">
	</div>
</div>

<br>

<div class="row">
	<div class="col-md-6">
		<img src="{{asset('konten/3.jpg')}}" class="shadow-box img-box-shadow">
	</div>
	<div class="col-md-6">
		<img src="{{asset('konten/4.jpg')}}" class="shadow-box img-box-shadow">
	</div>
</div>

<br>

<div class="row">
	<div class="col-md-6">
		<img src="{{asset('konten/5.jpg')}}" class="shadow-box img-box-shadow">
	</div>
	<div class="col-md-6">
		<img src="{{asset('konten/6.jpg')}}" class="shadow-box img-box-shadow">
	</div>
</div>

<br>

<div class="row">
	<div class="col-md-12">
		<p style="font-weight:bold; text-align:justify;">Sebuah merek perlu didaftarkan agar tidak diklaim dan digunakan oleh orang lain. Negara kita menerapkan sistem kepemilikan merek berasaskan <i>First to File</i>, yang berarti pemilik sah sebuah merek adalah pihak yang mendaftarkan merek tersebut pertama kali.</p>
		<br>
		<p style="font-weight:bold; text-align: justify;">Langkah penting sebelum mendaftarkan merek adalah melakukan pengecekan database merek Diejen HKI. Tujuannya untuk memastikan bahwa merek yang akan didaftarkan tidak memiliki kemiripan dengan merek yang sudah terdaftar atau sedang dalam proses permohonan oleh pihak lain. Proses pengecekan tersebut membutuhkan ketelitian tinggi sesuai dengan standar prosedur pemeriksaan oleh tenaga ahli Dirjen HKI.</p>
		<br>
		<p style="font-weight:bold; text-align: justify;">Setiap tahunnya ada ribuah permohonan marek yang ditolak karena melanggar ketentuan. Oleh karena itulah HALOPATEN hadir dengan tujuan mewakili masyarakat awam untuk menghandle seluruh proses permohonan merek agar terhindar dari penolakan merek oleh Dirjen HKI.</p>
		<br>
		<p style="font-weight:bold; text-align: justify;">Berdasarkan UU Merek, apabila permohonan merek ditolak maka uang yang telah disetorkan kepada negara tidak dapat ditarik kemabli. Namun kami memberikan garansi apabila permohonan merek melalui HALOPATEN ditolak, maka kami akan memberikan ganti rugi uang anda 100 persen tanpa syarat.</p>
		<br>
		<p style="font-weight:bold; text-align: justify;">Disertai dengan panduan step by step yang sangat lengkap, HALOPATEN membuat proses pendaftaran merek menjadi sangat mudah sehingga bisa dilakukan sendiri oleh siapa saja dari rumah dengan menggunakan smartphone atau komputer.</p>
		<br>
		<p style="font-weight:bold; text-align: justify;">Dengan berfokus pada layanan yang efisien, inovatif, dan dapat diakses oleh seluruh masyarat Indonesia, HALOPATEN berkomitmen menjadi garda terdepan dalam layanan pendaftaran merek di Indonesia.</p>
	</div>
</div>
@endsection

@section("css-tambahan")
@endsection

@section("js-tambahan")
@endsection