@extends("layouts.template")

@section("content")
<div class="row">
	<div class="col-md-12">
		<p class="bold">Frequently Asked Questions</p>
	</div>
</div>

<br>

<div class="row">
	<div class="col-md-12">
		<p class="blue-style bold">
			Bagaimana cara mendaftarkan merek melalui HALOPATEN?
		</p>
		<ol style="padding-left:10px;font-size:15px;">
			<li>1. Pilih menu Layanan</li>
			<li>2. Pilih Permohonan Merek Baru</li>
			<li>3. Isi data pemohon</li>
			<li>4. Isi data merek</li>
			<li>5. Isi data kelas</li>
			<li>6. Upload berkas</li>
			<li>7. Lakukan pembayaran</li>
			<li>8. Lakukan konfirmasi dengan upload bukti pembayaran</li>
			<li>9. Tim kami akan melakukan pengecekan merek menggunakan database Dirjen HKI</li>
			<li>10. Jika hasil pengecekan kososng, merek anda akan segera kami daftarkan. Jika hasil pengecekan ternyata ada kemiripan dengan merek lain, maka merek anda tidak bisa kami daftarkan. Silahkan lakukan penggantian nama merek.</li>
		</ol>
	</div>
</div>

<br>

<div class="row">
	<div class="col-md-12">
		<p class="blue-style bold">
			Mengapa wajib melakukan pengecekan merek sebelum mendaftarkannya?
		</p>
		<p>Pengecekan database merek wajib dilakukan sebelum mengajukan permohonan merek. Hal ini untuk menghindari adanya kemiripan dengan merek yang sudah terdaftar atau dalam proses permohonan oleh pihak lain. Kami yang akan melakukan proses pengecekan merek secara profesional & komprehensif.</p>
	</div>
</div>

<br>

<div class="row">
	<div class="col-md-12">
		<p class="blue-style bold">
			Bila setelah dilakukan pengecekan, ternyata merek tersebut tidak dapat didaftarkan. Apakah saya bisa membatalkan pemesanan dan menarik uang saya kembali?
		</p>
		<p>Mohon maaf. Setiap order tidak dapat dibatalkan dan uang pembayaran tidak dapat ditarik kemabli. Namun anda bisa mengganti dengan merek lain berkali-kali hingga mendapatkan merek yang bisa didaftarkan.</p>
	</div>
</div>

<br>

<div class="row">
	<div class="col-md-12">
		<p class="blue-style bold">
			Apa saja yang menyebabkan permohonan merek ditolak oleh Dirjen HKI?
		</p>
		<p>Setiap tahunnya ada ribuan permohonan merek ditolak. Berdasarkan UU merek</p>
		<ol style="padding-left:10px;font-size:15px;">
			<li>1. Tidak memiliki persamaan pada pokoknya dengan merek yang sudah ada.</li>
			<li>2. Tidak menggunakan nama orang terkenal.</li>
			<li>3. Tidak bertentangan dengan norma kesusilaan.</li>
			<li>4. Harus disertai oleh itikad baik, dengan kata lain merek yang dimohonkan adalah milik sendiri dan tidak mendompleng merek milik orang lain.</li>
		<p>Bila keempat syarat tersebut tidak dipenuhi, maka permohonan sebuah merek akan ditolak.</p>
	</div>
</div>

<br>

<div class="row">
	<div class="col-md-12">
		<p class="blue-style bold">
			Apabila permohonan merek saya ditolak, apakah uang saya akan dikembalikan?
		</p>
		<p>Berdasarkan UU Merek, bila permohonan merek ditolak maka uang yang telah dibayarkan kepada negara tidak dapat ditarik kembali. Namun kami memberikan garansi apabila permohonan merek melalui HALOPATEN ditolak, maka kami akan memberikan ganti rugi uang anda 100 persen.</p>
	</div>
</div>

<br>

<div class="row">
	<div class="col-md-12">
		<p class="blue-style bold">
			Berapa lama proses terbitnya sertifikat merek?
		</p>
		<p>Bisa memakan waktu hingga 18-24 bulan atau bahkan lebih. Namun perlindungan merek sudah berlaku setelah pemegang merek mengajukan permohonan merek dan mendapatkan nomor permohonan.</p>
	</div>
</div>

<br>

<div class="row">
	<div class="col-md-12">
		<p class="blue-style bold">
			Berapa lama masa berlaku merek yang telah saya daftarkan?
		</p>
		<p>Merek yang Anda daftarkan berlaku selama 10 tahun sejak nomor pendaftaran merek keluar.</p>
	</div>
</div>

<br>

<div class="row">
	<div class="col-md-12">
		<p class="blue-style bold">
			Bagaimana perlindungan merek yang saya dapatkan?
		</p>
		<p>Selama Sertifikat Merek belum terbit, pemohon tidak dapat mengambil tindakan hukum apapun terhadap pihak lain yang menggunakan merek tanpa ijin. Namun setelah Sertifikat Merek terbebut terbit, Pemegang Hak Merek dapat menuntut ganti kerugian atas pelanggaran merek yang dilakukan terhitung mulai dari Tanggal Permohonan.</p>
	</div>
</div>

<br>

<div class="row">
	<div class="col-md-12">
		<p class="blue-style bold">
			Apa saja tahapan penerbitan sertifikat merek?
		</p>
		<p>Berikut alur proses permohonan merek berdasarkan UU No. 20 Tahun 2016:</p>
		<p>
			<img src="{{asset('konten/proses_permohonan.png')}}">
		</p>
	</div>
</div>
@endsection

@section("css-tambahan")
@endsection

@section("js-tambahan")
@endsection