@extends("layouts.template")

@section("content")
<div class="row">
    <div class="col-md-12">
        <h4 class="bold">Kotak Pesan</h4>
    </div>
</div>

<br>

<form action="{{route('send_cusmes_from_cst')}}" method="POST">
    {{csrf_field()}}
    <div class="row">
        <div class="col-md-12">
            <label for="message_box">Pesan Saya:</label>
            <textarea class="form-control" id="message_box" rows="4" placeholder="Masukkan pesan Anda" required="" name="message"></textarea>
        </div>
        <div class="col-sm-12 right" style="margin-top:10px">
            <button class="btn btn-primary btm-md">
                <i class="fa fa-send"></i> Kirim
            </button>
        </div>
    </div>
</form>

<hr>

<div class="panel-collapse" id="collapseOne">
    <div class="panel-body">
        <ul class="chat">
            @foreach($data as $chat)
            @if($chat->is_answer)
            <li class="left clearfix"><span class="chat-img pull-left">
                @if($chat->sender==null)
                <img src="http://placehold.it/50/55C1E7/fff&text=A" alt="Admin Avatar" class="img-circle" />
                @else
                <img src="http://placehold.it/50/55C1E7/fff&text={{substr($chat->sender->name, 0, 1)}}" alt="{{$chat->sender->name}}" class="img-circle" />
                @endif
            </span>
                <div class="chat-body clearfix">
                    <div class="header">
                        @if($chat->sender==null)
                        <strong class="primary-font">Administrator</strong>
                        @else
                        <strong class="primary-font">{{$chat->sender->name}}</strong>
                        @endif
                        <small class="pull-right text-muted">
                            <span class="glyphicon glyphicon-time"></span>{{$chat->created_at}}
                        </small>
                    </div>
                    <p>
                        {!!nl2br(e($chat->message))!!}
                    </p>
                </div>
            </li>
            @else
            <li class="right clearfix"><span class="chat-img pull-right">
                <img src="http://placehold.it/50/FA6F57/fff&text={{substr($chat->customer->name, 0, 1)}}" alt="{{$chat->customer->name}}" class="img-circle" />
            </span>
                <div class="chat-body clearfix">
                    <div class="header">
                        <strong class="primary-font">{{$chat->customer->name}}</strong>
                        <small class="pull-left text-muted">
                            <span class="glyphicon glyphicon-time"></span>{{$chat->created_at}}
                        </small>
                    </div>
                    <p>
                        {!!nl2br(e($chat->message))!!}
                    </p>
                </div>
            </li>
            @endif
            @endforeach
        </ul>
    </div>
</div>

<br>

<div class="pagination text-center">
    {{ $data->links() }}
</div>
@endsection

@section("css-tambahan")
<style type="text/css">
.chat
{
    list-style: none;
    margin: 0;
    padding: 0;
}

.chat li
{
    margin-bottom: 10px;
    padding-bottom: 5px;
    border-bottom: 1px dotted #B3A9A9;
}

.chat li.left .chat-body
{
    margin-left: 60px;
}

.chat li.right .chat-body
{
    margin-right: 60px;
}


.chat li .chat-body p
{
    margin: 0;
    color: #777777;
}

.panel .slidedown .glyphicon, .chat .glyphicon
{
    margin-right: 5px;
}
</style>
@endsection

@section("js-tambahan")
@include("layouts.plugins.datatables")
@endsection