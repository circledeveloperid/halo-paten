<!DOCTYPE html>
<html>
    <head>
		<script type="text/javascript" src="{{asset('app-assets-custom\plugins/danied/content/--op-start-head.js')}}"></script>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="description" content="{{ env('APP_DESCRIPTION', '-') }}">
	    <meta name="keywords" content="{{ env('APP_KEYWORDS') }}">
	    <meta name="author" content="{{ env('APP_AUTHOR', 'Ahmad Saparudin') }}">
	    <title>{{ config('app.name', 'Ahmad Saparudin') }}</title>
	    <link rel="shortcut icon" type="image/x-icon" href="{{asset('app-assets/images/ico/favicon.ico')}}">
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="{{asset('app-assets-custom\plugins/danied/content/bootstrap.min.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('app-assets-custom\plugins/danied/content/style.css')}}">
        <script src="{{asset('app-assets-custom\plugins/danied/content/jquery-1.9.1.js')}}"></script>
        <script src="{{asset('app-assets-custom\plugins/danied/content/jquery-migrate-1.2.1.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('app-assets-custom\plugins/danied/content/--op-end-head.js')}}" defer></script>
</head>

    <body>
        <div class="container">
            <div class="col-md-6 col-sm-6 imgSec">
                <div class="icon">
                    <div class="victor"></div>
                    <div class="animation"></div>
                </div>
            </div>
            <div class="col-md-6 col-sm-6 content">
                <h2 class="heading">OOPS!</h2>
                <p>ACCESS DANIED</p>
                <p><small>Maaf, Anda tidak memiliki izin untuk mengakses halaman ini. Jika Anda merasa berhak, silahkan hubungi Admin.</small></p>
                <a href="{{route('home')}}" class="button"> Kembali</a>
            </div>
        </div>
    </body>
</html> 