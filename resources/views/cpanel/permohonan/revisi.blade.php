@extends("cpanel.layouts.app")

@section("link")
<div class="content-header-left col-md-6 col-12 mb-2">
  <h3 class="content-header-title">Permohonan Revisi</h3>
  <div class="row breadcrumbs-top">
    <div class="breadcrumb-wrapper col-12">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{Route('home')}}">Home</a></li>
        <li class="breadcrumb-item"><a href="#">Panel Permohonan</a></li>
        <li class="breadcrumb-item"><a href="#">Permohonan Merek Baru</a></li>
        <li class="breadcrumb-item active">Permohonan Revisi</li>
      </ol>
    </div>
  </div>
</div>
@endsection

@section("konten")
<section id="complex-header">
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-header">
          <h4 class="card-title">Daftar Permohonan Revisi</h4>
          <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
          <div class="heading-elements">
            <ul class="list-inline mb-0">
              <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
              <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
              <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
              <li><a data-action="close"><i class="ft-x"></i></a></li>
            </ul>
          </div>
        </div>
        <div class="card-content collapse show">
          <div class="card-body card-dashboard">
            <table class="table table-striped table-bordered column-visibility" id="tabel_permohonan_masuk" style="width:100%">
              <thead>
                <tr>
                  <!-- <th>No</th>
                  <th>Invoice</th>
                  <th>Nama Pemohon</th>
                  <th>Merek</th>
                  <th>Jumlah Kelas</th>
                  <th>Total Biaya</th>
                  <th>Aksi</th> -->
                    <th>No.</th>
                    <th>Invoice#</th>
                    <th>Nama Pemohon</th>
                    <th>Merek</th>
                    <th>Jumlah Kelas</th><!-- 
                    <th>Total Biaya</th>
                    <th>Status Pembayaran</th>
                    <th>Konfirmasi Pembayaran</th> -->
                    <th>Nomor Permohonan</th>
                    <th>Bukti Permohonan</th>
                    <th>Unduh</th>
                    <th>Status Permohonan</th>
                </tr>
              </thead>
              <tbody>
                <tr class="center">
                  <td colspan="9">Mengambil data...</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<form action="#" method="POST" id="upload_tanda_terima" style="display:none">
    <input type="file" name="photo" onchange="upload_tanda_terima()" accept="image/*,.PDF" id="choose_file_tanda_terima">
</form>

<form action="#" method="POST" id="upload_surat_pernyataan" style="display:none">
    <input type="file" name="photo" onchange="upload_surat_pernyataan()" accept="image/*,.PDF" id="choose_file_surat_pernyataan">
</form>

<script type="text/javascript">
    $(document).ready( function () {
        $tabel_permohonan_masuk = $("#tabel_permohonan_masuk").DataTable({
            //"pageLength": 1, //untuk debuging
            "scrollX" : true,
            "ajax": {
                url: "{{Route('service')}}/getPermohonanRevisiMasuk"
            },
            "processing": true,
            "serverSide": true,
            "columns": [
                {"data": "id_permohonan", render: function (data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1;
                }, searchable: false},
                {"data": "id_permohonan", render: function (data, type, row, meta) {
                    return "<a href='{{route('invoice')}}/"+data+"' target='_BLANK'>INV"+data+"</a>";
                }, searchable: false},
                { "data": "data_pemohon.nama_pemohon"},
                { "data": "data_merek.merek" },
                { "data": "jumlah_kelas", render: function (data, type, row, meta) {
                    return data+" kelas";
                }},
               /* { "data": "harga", searchable: false, orderable: false},
                { "data": "status_billing", searchable: false, orderable: false},
                { "data": "pembayaran_dikonfirmasi", render: function(data, type, row, meta){
                    $bukti = row['bukti_pembayaran'];
                    $konfirm = data;
                    if(($bukti==null || $bukti=="") && ($konfirm==true || $konfirm==1 || $konfirm=="1") && row['payment_id']!=null && row['payment_id']!="")
                        return "<button class='btn btn-sm btn-danger' disabled='disabled' title='Auto konfirmasi oleh sistem'><i class='la la-times'></i> Unkonfirmasi</button>"; 
                    if($bukti==null || $bukti=="")
                        return "<button class='btn btn-sm btn-success' disabled='disabled' title='Bukti pembayaran belum diunggah oleh pengguna'><i class='la la-check'></i> Konfirmasi</button>";
                    if($bukti!=null && $bukti!="" && $konfirm==0)
                        return "<button class='btn btn-sm btn-success' onclick='konfirmasi_pembayaran("+row['id_permohonan']+")'><i class='la la-check'></i> Konfirmasi</button>";
                    else
                        return "<button class='btn btn-sm btn-danger' onclick='unkonfirmasi_pembayaran("+row['id_permohonan']+")'><i class='la la-times'></i> Unkonfirmasi</button>";
                }, searchable: false, orderable: false},*/
                { "data": "nomor_permohonan", render: function (data, type, row, meta) {
                    if(data==null || data=="")
                        return "<input type='text' title='Masukkan nomor permohonan' placeholder='Masukkan no. permohonan' class='form-control text-in-table' onkeyup='update_nomor_permohonan("+row['id_permohonan']+",this.value)'>";
                    else
                        return "<input type='text' title='Masukkan nomor permohonan' placeholder='' value='"+data+"' class='form-control text-in-table' onkeyup='update_nomor_permohonan("+row['id_permohonan']+",this.value)'>";
                }},
                { "data": "id_permohonan", render: function(data, type, row, meta){
                    $btn_tanda_terima = "<button class='btn btn-sm btn-info' onclick='choose_file_tanda_terima("+data+")'><i class='la la-upload'></i> Tanda Terima</button>";
                    //$btn_surat_pernyataan = "<button class='btn btn-sm btn-primary' onclick='choose_file_surat_pernyataan("+data+")'><i class='la la-upload'></i> Surat Pernyataan</button>";
                    $btn_surat_pernyataan = "";
                    return $btn_tanda_terima+"<div></div>"+$btn_surat_pernyataan;
                }, searchable: false, orderable: false},
                { "data": "id_permohonan", render: function(data, type, row, meta){
                    if(row['bukti_pembayaran']!=null && row['bukti_pembayaran']!="")
                        $btn_bukti_pembayaran = "<a href='{{route('root')}}/"+row['bukti_pembayaran']+"' target='_BLANK' class='white btn btn-sm btn-warning'><i class='la la-download'></i> Bukti Pembayaran</a>";
                    else
                        $btn_bukti_pembayaran = "";
                    if(row['tanda_terima']!=null && row['tanda_terima']!="")
                        $btn_tanda_terima = "<a href='{{route('root')}}/"+row['tanda_terima']+"' target='_BLANK' class='white btn btn-sm btn-info'><i class='la la-download'></i> Tanda Terima</a>";
                    else
                        $btn_tanda_terima = "";

                    if(row['surat_pernyataan']!=null && row['surat_pernyataan']!="")
                        //$btn_surat_pernyataan = "<a href='{{route('root')}}/"+row['surat_pernyataan']+"' target='_BLANK' class='white btn btn-sm btn-primary'><i class='la la-download'></i> Surat Pernyataan</a>";
                        $btn_surat_pernyataan = "";
                    else
                        $btn_surat_pernyataan = "";
                    if( $btn_bukti_pembayaran=="" && $btn_tanda_terima=="" && $btn_surat_pernyataan=="") return "-";
                    return $btn_bukti_pembayaran+" "+$btn_tanda_terima+" "+$btn_surat_pernyataan;
                }, searchable: false, orderable: false},
                { "data": "status_permohonan.status", render: function(data, type, row, meta){
                    $default = row['id_status_permohonan'];
                    $id_permohonan = row['id_permohonan'];
                    $res = "<select class='form-control text-in-table' onchange='update_status_permohonan("+$id_permohonan+",this.value)' id='status-permohonan-"+$id_permohonan+"'>";
                    @foreach($status_permohonan as $val)
                    if($default == parseInt("{{$val->id_status_permohonan}}"))
                        $res+="<option value='{{$val->id_status_permohonan}}' selected>{{$val->status}}</option>";
                    else
                        $res+="<option value='{{$val->id_status_permohonan}}'>{{$val->status}}</option>";
                    @endforeach
                    $res+= "</select>";
                    return $res;
                }, orderable: false, searchable: false},
            ],
            "order": [[1, 'desc']]
        });
    });
    
    function konfirmasi_pembayaran($id_permohonan) {
        showLoading();
        $.ajax({
            url    : "{{Route('handle_permohonan_baru')}}/konfirmasi_pembayaran/"+$id_permohonan,
            method : "POST",
            headers: {
                "token"    : "{{Crypt::encrypt(auth::user()->id)}}",
                'X-CSRF-TOKEN': "{{csrf_token()}}",
            },
            data   : {

            },
            success: function(res) {
                hideLoading();
                if(res.success) {
                    miniNotif("success",res.pesan);
                    $("#tabel_permohonan_masuk").DataTable().ajax.reload();
                } else
                    miniNotif("error",res.pesan);
            },
            error  : function() {
                hideLoading();
                miniNotif("error","Tidak dapat terhubung ke server!");
            }
        })
    }

    function unkonfirmasi_pembayaran($id_permohonan) {
        showLoading();
        $.ajax({
            url    : "{{Route('handle_permohonan_baru')}}/unkonfirmasi_pembayaran/"+$id_permohonan,
            method : "POST",
            headers: {
                "token"    : "{{Crypt::encrypt(auth::user()->id)}}",
                'X-CSRF-TOKEN': "{{csrf_token()}}",
            },
            data   : {

            },
            success: function(res) {
                hideLoading();
                if(res.success) {
                    miniNotif("success",res.pesan);
                    $("#tabel_permohonan_masuk").DataTable().ajax.reload();
                } else
                    miniNotif("error",res.pesan);
            },
            error  : function() {
                hideLoading();
                miniNotif("error","Tidak dapat terhubung ke server!");
            }
        })
    }

    function update_status_permohonan($id_permohonan,$id_status) {
        showLoading();
        $.ajax({
            url    : "{{Route('handle_permohonan_baru')}}/update_status_permohonan",
            method : "POST",
            headers: {
                "token"    : "{{Crypt::encrypt(auth::user()->id)}}",
                'X-CSRF-TOKEN': "{{csrf_token()}}",
            },
            data   : {
                "id_permohonan" : $id_permohonan,
                "id_status" : $id_status,
            },
            success: function(res) {
                hideLoading();
                if(res.success) {
                    miniNotif("success",res.pesan);
                    $("#tabel_permohonan_masuk").DataTable().ajax.reload();
                } else
                    miniNotif("error",res.pesan);
            },
            error  : function() {
                hideLoading();
                miniNotif("error","Tidak dapat terhubung ke server!");
            }
        })
    }

    function update_nomor_permohonan($id_permohonan,$no_permohonan) {
        $.ajax({
            url    : "{{Route('handle_permohonan_baru')}}/set_nomor_permohonan",
            method : "POST",
            headers: {
                "token"    : "{{Crypt::encrypt(auth::user()->id)}}",
                'X-CSRF-TOKEN': "{{csrf_token()}}",
            },
            data   : {
                "id_permohonan" : $id_permohonan,
                "nomor_permohonan" : $no_permohonan,
            },
            success: function(res) {
                // No Action
            },
            error  : function() {
                hideLoading();
                miniNotif("error","Tidak dapat terhubung ke server!");
            }
        })
    }

    $tmp_target_permohonan = null;

    function choose_file_tanda_terima($id_permohonan) {
        $tmp_target_permohonan = $id_permohonan;
        $("#upload_tanda_terima input").click();
    }

    function choose_file_surat_pernyataan($id_permohonan) {
        $tmp_target_permohonan = $id_permohonan;
        $("#upload_surat_pernyataan input").click();
    }

    function upload_tanda_terima() {
        showLoading();
        $file_data = $("#choose_file_tanda_terima").prop('files')[0];
        $form_data = new FormData();
        $form_data.append('tanda_terima',$file_data);
        $form_data.append('id_permohonan',$tmp_target_permohonan);
        $.ajax({
            url    : "{{Route('handle_permohonan_baru')}}/upload_tanda_terima",
            method : "POST",
            cache  : false,
            contentType: false,
            processData: false,
            headers: {
                "token"    : "{{Crypt::encrypt(auth::user()->id)}}",
                'X-CSRF-TOKEN': "{{csrf_token()}}",
            },
            data   : $form_data,
            success: function(res) {
                hideLoading();
                if(res.success) {
                    miniNotif("success",res.pesan);
                    $("#tabel_permohonan_masuk").DataTable().ajax.reload();
                } else
                    miniNotif("error",res.pesan);
            },
            error  : function() {
                hideLoading();
                miniNotif("error","Tidak dapat terhubung ke server!");
            }
        })
    }

    function upload_surat_pernyataan() {
        showLoading();
        $file_data = $("#choose_file_surat_pernyataan").prop('files')[0];
        $form_data = new FormData();
        $form_data.append('surat_pernyataan',$file_data);
        $form_data.append('id_permohonan',$tmp_target_permohonan);
        $.ajax({
            url    : "{{Route('handle_permohonan_baru')}}/upload_surat_pernyataan",
            method : "POST",
            cache  : false,
            contentType: false,
            processData: false,
            headers: {
                "token"    : "{{Crypt::encrypt(auth::user()->id)}}",
                'X-CSRF-TOKEN': "{{csrf_token()}}",
            },
            data   : $form_data,
            success: function(res) {
                hideLoading();
                if(res.success) {
                    miniNotif("success",res.pesan);
                    $("#tabel_permohonan_masuk").DataTable().ajax.reload();
                } else
                    miniNotif("error",res.pesan);
            },
            error  : function() {
                hideLoading();
                miniNotif("error","Tidak dapat terhubung ke server!");
            }
        })
    }
</script>
@endsection

@section("plugin")
<link rel="stylesheet" type="text/css" href="{{asset('app-assets/vendors/css/tables/datatable/datatables.min.css')}}">
<!-- Drag And Drop -->
<link rel="stylesheet" type="text/css" href="{{asset('app-assets/vendors/css/forms/toggle/switchery.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('app-assets/vendors/css/ui/dragula.min.css')}}">
@endsection

@section("script")
<script src="{{asset('app-assets/vendors/js/tables/datatable/datatables.min.js')}}" type="text/javascript"></script>
<!-- Drag And Drop -->
<script src="{{asset('app-assets/vendors/js/forms/toggle/switchery.min.js')}}" type="text/javascript"></script>
<script src="{{asset('app-assets/vendors/js/extensions/dragula.min.js')}}" type="text/javascript"></script>
@endsection