@extends("cpanel.layouts.app")

@section("link")
<div class="content-header-left col-md-6 col-12 mb-2">
  <h3 class="content-header-title">Daftar Karyawan</h3>
  <div class="row breadcrumbs-top">
    <div class="breadcrumb-wrapper col-12">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{Route('home')}}">Home</a></li>
        <li class="breadcrumb-item"><a href="#">Panel Management</a></li>
        <li class="breadcrumb-item"><a href="#">Data Karyawan</a></li>
        <li class="breadcrumb-item active">Daftar Karyawan</li>
      </ol>
    </div>
  </div>
</div>
@endsection

@section("konten")
<section id="complex-header">
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-header">
          <h4 class="card-title">Daftar Karyawan</h4>
          <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
          <div class="heading-elements">
            <ul class="list-inline mb-0">
              <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
              <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
              <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
              <li><a data-action="close"><i class="ft-x"></i></a></li>
            </ul>
          </div>
        </div>
        <div class="card-content collapse show">
          <div class="card-body card-dashboard">
            <table class="table table-striped table-bordered column-visibility" id="my_table">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Nama Lengkap</th>
                  <th>Alamat Email</th>
                  <th>Nomor Ponsel</th>
                  <th>Aksi</th>
                </tr>
              </thead>
              <tbody>
                <tr class="center">
                  <td colspan="5">Mengambil data...</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<script type="text/javascript">
  $(document).ready(function(){
      var datatable = $('#my_table').DataTable({
        "language": {searchPlaceholder: "Nama / Username"},
        "ajax": "{{Route('get_karyawan')}}",
        "processing": true,
        "serverSide": true,
        "columns": [
            { "data": "id", render: function (data, type, row, meta) {
              return meta.row + meta.settings._iDisplayStart + 1;
            }, searchable: false},
            { "data": "name" },
            { "data": "email" },
            { "data": "ponsel" },
            { "data": "id", render: function (data, type, row, meta) {
                $btn_edit = "<a class='btn btn-sm btn-info' href='{{route('karyawan_edit')}}/"+data+"' ><i class='la la-edit'></i> Edit</a>";
                $btn_hapus = "<button class='btn btn-sm btn-danger' onclick='hapus("+data+")'><i class='la la-trash'></i> Hapus</button>";
                return $btn_edit+" | "+$btn_hapus;
            }, "searchable": false, "orderable": false},
        ],
        "order": [[0, 'asc']]
    });
  });
</script>

<!-- Modal -->
<div class="modal fade text-left col-md" id="modal_hapus" tabindex="-1" role="dialog" aria-labelledby="myModal" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header bg-danger white">
        <h4 class="modal-title white" id="myModal">
            <i class="la la-trash"></i> Hapus Data
        </h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h5><i class="la la-question-circle"></i> Yakin akan menghapus karayawan tersebut?</h5>
        <p>Karyawan yang telah dihapus tidak dapat dikembalikan lagi (kecuali ditambahkan ulang)!</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">
          Batal
        </button>
        <button type="button" class="btn btn-danger" onclick="do_delete()">
          <i class="la la-trash"></i> Yakin, Hapus
        </button>
      </div>
    </div>
  </div>
</div>
<!-- Modal -->
<script type="text/javascript">
  $tmp_id = 0;
  $index_target = 0;
  function hapus($id) {
    $tmp_id = $id;
    $("#modal_hapus").modal("show");
  }
  function do_delete() {
    showLoading();
    $.ajax({
      url    : "{{Route('pengguna.store')}}/"+$tmp_id,
      method : "DELETE",
      headers: {
        "token"    : "{{Crypt::encrypt(auth::user()->id)}}",
      },
      success: function(res) {
        hideLoading();
        if(res.success) {
          miniNotif("success",res.pesan);
          after_delete();
        } else
          miniNotif("error",res.pesan);
      },
      error  : function() {
        hideLoading();
        miniNotif("error","Tidak dapat terhubung ke server!");
      }
    })
    function after_delete() {
      $("#modal_hapus").modal("hide");
      $("#my_table").DataTable().ajax.reload();
    }
  }
</script>
@endsection

@section("plugin")
<link rel="stylesheet" type="text/css" href="{{asset('app-assets/vendors/css/tables/datatable/datatables.min.css')}}">
<!-- Drag And Drop -->
<link rel="stylesheet" type="text/css" href="{{asset('app-assets/vendors/css/forms/toggle/switchery.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('app-assets/vendors/css/ui/dragula.min.css')}}">
@endsection

@section("script")
<script src="{{asset('app-assets/vendors/js/tables/datatable/datatables.min.js')}}" type="text/javascript"></script>
<!-- Drag And Drop -->
<script src="{{asset('app-assets/vendors/js/forms/toggle/switchery.min.js')}}" type="text/javascript"></script>
<script src="{{asset('app-assets/vendors/js/extensions/dragula.min.js')}}" type="text/javascript"></script>
@endsection