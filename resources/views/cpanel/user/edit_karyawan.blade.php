@extends("cpanel.layouts.app")

@section("link")
<div class="content-header-left col-md-6 col-12 mb-2">
  <h3 class="content-header-title">Edit Karyawan</h3>
  <div class="row breadcrumbs-top">
    <div class="breadcrumb-wrapper col-12">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{Route('home')}}">Home</a></li>
        <li class="breadcrumb-item"><a href="#">Panel Management</a></li>
        <li class="breadcrumb-item"><a href="{{Route('karyawan_list')}}">Data Karyawan</a></li>
        <li class="breadcrumb-item active">Edit Karyawan - {{$karyawan->name}}</li>
      </ol>
    </div>
  </div>
</div>
<div class="content-header-right col-md-6 col-12">
  <div class="btn-group float-md-right white" role="group" aria-label="Button group with nested dropdown">
  </div>
</div>
@endsection

@section("konten")
<section id="basic-form-layouts">
  <div class="row match-height">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          <h4 class="card-title" id="basic-layout-form">Edit Karyawan</h4>
          <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
          <div class="heading-elements">
            <ul class="list-inline mb-0">
              <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
              <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
              <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
              <li><a data-action="close"><i class="ft-x"></i></a></li>
            </ul>
          </div>
        </div>
        <div class="card-content collapse show">
          <div class="card-body">
            <div class="card-text">
            </div>
            <form class="form" action="#" method="POST" onsubmit="return false">
              <div class="form-body">
                <!-- Row Item -->
                <h4 class="form-section"><i class="la la-file-o"></i> User Details</h4>
                <!-- Row Item -->
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="projectinput1">Nama Lengkap</label>
                      <input type="text" name="" id="name" class="form-control" placeholder="Contoh: John Gilbert" maxlength="20" value="{{$karyawan->name}}">
                    </div>
                    <div class="form-group">
                      <label for="projectinput1">Alamat Email</label>
                      <input type="text" name="" id="email" class="form-control" placeholder="Contoh: johngilbert@gmail.com" onkeypress="return validasi_email(event)" maxlength="30" value="{{$karyawan->email}}">
                    </div>
                    <div class="form-group">
                      <label for="projectinput1">Nomor Ponsel</label>
                      <input type="text" name="" id="ponsel" class="form-control" placeholder="Contoh: 082282170800" value="{{$karyawan->ponsel}}">
                    </div>
                  </div>
                </div>
              <div class="form-actions right">
                <a type="button" class="btn btn-warning mr-1" href="{{url()->current()}}">
                  <i class="la la-refresh"></i> Reset Inputan
                </a>
                <button type="button" class="btn btn-primary" onclick="lanjutkan()">
                  <i class="la la-save"></i> Simpan Data
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<script type="text/javascript">
    function disable_input() {
      $("#name").attr("disabled","disabled");
      $("#email").attr("disabled","disabled");
      $("#ponsel").attr("disabled","disabled");
    }

    function lanjutkan() {
    $name = $("#name").val();
    $email = $("#email").val();
    $ponsel = $("#ponsel").val();
  
    if(!handle_require($name,"Nama belum diinput")) return;
    if(!handle_require($email,"Email belum diinput")) return;
    showLoading();
    $.ajax({
      url    : "{{Route('pengguna.store')}}/{{$karyawan->id}}",
      method : "PUT",
      headers: {
        "token"    : "{{Crypt::encrypt(auth::user()->id)}}",
      },
      data   : {
        "name" : $name,
        "email" : $email,
        "ponsel" : $ponsel,
      },
      success: function(res) {
        hideLoading();
        if(res.success) {
          miniNotif("success",res.pesan);
          disable_input();
          redirect("{{Route('karyawan_list')}}",3000);
        } else
          miniNotif("error",res.pesan);
      },
      error  : function() {
        hideLoading();
        miniNotif("error","Tidak dapat terhubung ke server!");
      }
    })
  }

  function handle_require($val,$error) {
    if($val=="" || $val==null) {
      miniNotif("error",$error);
      return false;
    } else
      return true;
  }
</script>
@endsection

@section("plugin")
<link rel="stylesheet" type="text/css" href="{{asset('app-assets-custom/css/jBox.all.css')}}">
@endsection

@section("script")
<script type="text/javascript" src="{{asset('app-assets-custom/js/jBox.all.js')}}"></script>
<script type="text/javascript" src="{{asset('app-assets-custom/js/jBox-set.js')}}"></script>
<script src="{{asset('app-assets/vendors/js/editors/ckeditor/ckeditor.js')}}" type="text/javascript"></script>
<script src="{{asset('app-assets-custom\plugins/auth/js/inputan.js')}}" type="text/javascript"></script>
@endsection