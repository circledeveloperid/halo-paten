@extends("cpanel.layouts.app")

@section("link")
<div class="content-header-left col-md-6 col-12 mb-2">
  <h3 class="content-header-title">Pengaturan Role API</h3>
  <div class="row breadcrumbs-top">
    <div class="breadcrumb-wrapper col-12">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{Route('home')}}">Home</a>
        </li>
        <li class="breadcrumb-item"><a href="#">Pengaturan</a>
        </li>
        <li class="breadcrumb-item active">Role API
        </li>
      </ol>
    </div>
  </div>
</div>
@endsection

@section("konten")
<section id="complex-header">
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-header">
          <h4 class="card-title">Daftar Role</h4>
          <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
          <div class="heading-elements">
            <ul class="list-inline mb-0">
              <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
              <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
              <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
              <li><a data-action="close"><i class="ft-x"></i></a></li>
            </ul>
          </div>
        </div>
        <div class="card-content collapse show">
          <div class="card-body card-dashboard">
            <p class="card-text">Pengaturan yang ditetapkan pada laman ini berlaku pada sistem informasi "{{env("APP_NAME","Circle")}}" berserta turunannya.</p>
            <table class="table table-striped table-bordered column-visibility" id="my_table">
              <thead>
                <tr>
                  <th>Nama Rule</th>
                  <th>Deskripsi</th>
                  <th>Aksi</th>
                </tr>
              </thead>
              <tbody>
                @if(count($role)==0)
                <tr class="center">
                  <td colspan="3">Tidak ada Role</td>
                </tr>
                @endif
                @foreach($role as $index => $value)
                <tr>
                  <td>{{$value->role_name}}</td>
                  <td>{{$value->description}}</td>
                  <td>
                      <button class="btn btn-md btn-primary" onclick="manageMenu('{{$value->id}}','{{$value->role_name}}')">
                        <i class="la la-cog"></i> Manage API
                      </button>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<script type="text/javascript">
  $(document).ready(function(){

  });
</script>

<!-- Modal -->
<div class="modal fade text-left" id="modal_utama" tabindex="-1" role="dialog" aria-labelledby="myModal" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header bg-success white">
        <h4 class="modal-title white" id="myModal">
            <i class="la la-cog"></i> Manajemen Role Menu
        </h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h5><i class="la la-lightbulb-o"></i> Tips dan Informasi</h5>
        <p>Ceklis checkbox pada kolom "Akses" untuk memberikan akses menu kepada pengguna dengan Rule: <span class="bold" id="role_name">Admin</span>. Untuk menghapus akses menu kepada Role yang bersangkutan, cukup unceklis checkbox.</p>
        <hr>
        <div class="col-md-12">
          <div class="card">
            <div class="card-content">
              <div class="card-body">
                <div class="row skin skin-square">
                  <table class="table tabel-ku">
                    <thead>
                      <tr>
                        <th>Fungsi API</th>
                        <th>Route API</th>
                        <th>Deskripsi</th>
                        <th>Akses</th>
                      </tr>
                    </thead>
                    <tbody class="listMenu">
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">
          Tutup
        </button>
        <button type="button" class="btn btn-outline-success" onclick="saveRoleMenu()">
          Simpan Perubahan
        </button>
        <input type="hidden" name="" id="target_id">
      </div>
    </div>
  </div>
</div>
<!-- Modal -->
<script type="text/javascript">
  $(document).ready(function(){
      
  })
  function initializeComponent() {
    $('.skin-square input').iCheck({
        checkboxClass: 'icheckbox_square-red',
        radioClass: 'iradio_square-red',
    });
    $(".tabel-ku").DataTable({
      "lengthMenu": [[3, 5, 10, -1], [3, 5, 10, "All"]],
      "pageLength" : 3,
      "columns": [
        null,
        null,
        null,
        { "orderable": false },
        ],
    });
  }
  function manageMenu($id_role,$role_name) {
    $("#target_id").val($id_role);
    $("#role_name").html($role_name);
    getServerData($id_role);
  }
  function getServerData($id_role) {
      showLoading();
      $.ajax({
          url     : "{{Route('role_api.index')}}/"+$id_role,
          headers : {
              "token" : "{{Crypt::encrypt(Auth::user()->id)}}",
          },
          success : function(res) {
              hideLoading();
              if(res.success) {
                  $(".tabel-ku").dataTable().fnDestroy(); // hancurkan datatable
                  printMenu(res.data,".listMenu");
                  initializeComponent();
                  $("#modal_utama").modal("show");
              } else
                miniNotif("error",res.pesan);
          },
          error   : function() {
              hideLoading();
              miniNotif("error","Koneksi ke server bermasalah!");
          }
      })
  }
  function saveRoleMenu() {
      $id_role  = $("#target_id").val();
      showLoading();
      $.ajax({
          url     : "{{Route('role_api.store')}}",
          method  : "POST",
          headers : {
              "token" : "{{Crypt::encrypt(Auth::user()->id)}}",
          },
          data    : {
              "id_role" : $id_role,
              "APIs"    : getRoleMenu(),
          },
          success : function(res) {
            hideLoading();
            if(res.success) {
              miniNotif("success",res.pesan);
              $("#modal_utama").modal("hide");
            } else
              miniNotif("error",res.pesan);
          },
          error   : function() {
            hideLoading();
            miniNotif("error","Tidak dapat terhubung ke server!");
          }
      })
  }
  function getRoleMenu() {
      $role = [];
      $checked_row =  $(".tabel-ku").dataTable().$(".haveAccess:checked", {"page": "all"});
      $checked_row.each(function(){
          $id_role = $(this).attr("id-menu");
          $role.push($id_role);
      });
      return $role;
  }
  function printMenu($data,$target) {
      $print = "";
      for($i=0;$i<$data.length;$i++) {
        $print+='<tr>';
        $print+='<td>'+$data[$i].api+'</td>';
        $print+='<td>'+$data[$i].routeName+'</td>';
        $print+='<td>'+$data[$i].description+'</td>';
        if($data[$i].haveAccess)
          $print+='<td><input type="checkbox" class="haveAccess" id-menu="'+$data[$i].id+'" checked></td>';
        else
          $print+='<td><input type="checkbox" class="haveAccess" id-menu="'+$data[$i].id+'"></td>';
        $print+='</tr>';
      };
      $($target).html($print);
  }
</script>
@endsection

@section("plugin")
<link rel="stylesheet" type="text/css" href="{{asset('app-assets/vendors/css/tables/datatable/datatables.min.css')}}">
<!-- Checkbox -->
<link rel="stylesheet" type="text/css" href="{{asset('app-assets/vendors/css/forms/icheck/icheck.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('app-assets/vendors/css/forms/icheck/custom.css')}}">
@endsection

@section("script")
<script src="{{asset('app-assets/vendors/js/tables/datatable/datatables.min.js')}}" type="text/javascript"></script>
<!-- Checkbox -->
<script src="{{asset('app-assets/vendors/js/forms/icheck/icheck.min.js')}}" type="text/javascript"></script>
@endsection