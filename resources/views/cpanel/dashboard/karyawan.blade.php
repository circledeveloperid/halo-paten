@extends("cpanel.layouts.app")

@section("konten")
<div class="row">
  <div class="col-xl-3 col-lg-6 col-12">
    <div class="card pull-up">
      <a href="{{route('user_list')}}">
        <div class="card-content">
          <div class="card-body">
            <div class="media d-flex">
              <div class="media-body text-left">
                <h3 class="info capitalize">{{number_format($jml_customer)}} Orang</h3>
                <h6>Jumlah Customer</h6>
              </div>
              <div>
                <i class="la la-users info font-large-2 float-right"></i>
              </div>
            </div>
          </div>
        </div>
      </a>
    </div>
  </div>
  <div class="col-xl-3 col-lg-6 col-12">
    <div class="card pull-up">
      <a href="{{route('permohonan_pending')}}">
        <div class="card-content">
          <div class="card-body">
            <div class="media d-flex">
              <div class="media-body text-left">
                <h3 class="warning">
                  {{number_format($jml_p_pending)}} Perm.
                </h3>
                <h6>Jumlah Permohonan Pending</h6>
              </div>
              <div>
                <i class="la la-exclamation-circle warning font-large-2 float-right"></i>
              </div>
            </div>
          </div>
        </div>
      </a>
    </div>
  </div>
  <div class="col-xl-3 col-lg-6 col-12">
    <div class="card pull-up">
      <a href="{{route('permohonan_masuk')}}">
        <div class="card-content">
          <div class="card-body">
            <div class="media d-flex">
              <div class="media-body text-left">
                <h3 class="primary">
                  {{number_format($jml_p_proses)}} Perm.
                </h3>
                <h6>Jumlah Permohonan Diproses</h6>
              </div>
              <div>
                <i class="la la-hourglass primary font-large-2 float-right"></i>
              </div>
            </div>
          </div>
        </div>
      </a>
    </div>
  </div>
  <div class="col-xl-3 col-lg-6 col-12">
    <div class="card pull-up">
      <a href="{{route('permohonan_berhasil')}}">
        <div class="card-content">
          <div class="card-body">
            <div class="media d-flex">
              <div class="media-body text-left">
                <h3 class="success">
                  {{number_format($jml_p_selesai)}} Perm.
                </h3>
                <h6>Jumlah Permohonan Selesai</h6>
              </div>
              <div>
                <i class="la la-check success font-large-2 float-right"></i>
              </div>
            </div>
          </div>
        </div>
      </a>
    </div>
  </div>
</div>
@endsection

@section("plugin")
<link rel="stylesheet" type="text/css" href="{{asset('app-assets/vendors/css/tables/datatable/datatables.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('modules/daterangepicker/daterangepicker.css')}}" />
@endsection

@section("script")
<script src="{{asset('app-assets/vendors/js/tables/datatable/datatables.min.js')}}" type="text/javascript"></script>
<script type="text/javascript" src="{{asset('modules/daterangepicker/moment.min.js')}}"></script>
<script type="text/javascript" src="{{asset('modules/daterangepicker/daterangepicker.min.js')}}"></script>
@endsection