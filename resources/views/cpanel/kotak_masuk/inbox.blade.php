@extends("cpanel.layouts.app")

@section("link")
<div class="content-header-left col-md-6 col-12 mb-2">
  <h3 class="content-header-title">Kotak Masuk</h3>
  <div class="row breadcrumbs-top">
    <div class="breadcrumb-wrapper col-12">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{Route('home')}}">Home</a></li>
        <li class="breadcrumb-item"><a href="#">Panel Ekstra</a></li>
        <li class="breadcrumb-item"><a href="#">Kotak Masuk</a></li>
        <li class="breadcrumb-item active">Kotak Masuk</li>
      </ol>
    </div>
  </div>
</div>
@endsection

@section("konten")
<section id="complex-header">
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-header">
          <h4 class="card-title">Daftar Kotak Masuk</h4>
          <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
          <div class="heading-elements">
            <ul class="list-inline mb-0">
              <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
              <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
              <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
              <li><a data-action="close"><i class="ft-x"></i></a></li>
            </ul>
          </div>
        </div>
        <div class="card-content collapse show">
          <div class="card-body card-dashboard">
            <table class="table table-striped table-bordered column-visibility" id="my_table">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Nama Lengkap</th>
                  <th>Alamat Email</th>
                  <th>Pertanyaan / Pernyataan</th>
                </tr>
              </thead>
              <tbody>
                <tr class="center">
                  <td colspan="4">Mengambil data...</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<script type="text/javascript">
  $(document).ready(function(){
      var datatable = $('#my_table').DataTable({
        "language": {searchPlaceholder: "Nama / Username"},
        "ajax": "{{Route('service')}}/getKritikSaran",
        "processing": true,
        "serverSide": true,
        "columns": [
            { "data": "id_kritik_saran", render: function (data, type, row, meta) {
              return meta.row + meta.settings._iDisplayStart + 1;
            }, searchable: false},
            { "data": "name" },
            { "data": "email" },
            { "data": "deskripsi" },/*
            { "data": "id", render: function (data, type, row, meta) {
                return "<button class='btn btn-sm btn-danger' onclick='hapus("+data+")'><i class='la la-trash'></i> Hapus</button>";
            }, "searchable": false, "orderable": false},*/
        ],
        "order": [[0, 'asc']]
    });
  });
</script>
@endsection

@section("plugin")
<link rel="stylesheet" type="text/css" href="{{asset('app-assets/vendors/css/tables/datatable/datatables.min.css')}}">
<!-- Drag And Drop -->
<link rel="stylesheet" type="text/css" href="{{asset('app-assets/vendors/css/forms/toggle/switchery.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('app-assets/vendors/css/ui/dragula.min.css')}}">
@endsection

@section("script")
<script src="{{asset('app-assets/vendors/js/tables/datatable/datatables.min.js')}}" type="text/javascript"></script>
<!-- Drag And Drop -->
<script src="{{asset('app-assets/vendors/js/forms/toggle/switchery.min.js')}}" type="text/javascript"></script>
<script src="{{asset('app-assets/vendors/js/extensions/dragula.min.js')}}" type="text/javascript"></script>
@endsection