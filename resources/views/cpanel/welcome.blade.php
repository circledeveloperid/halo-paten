<!DOCTYPE html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8">
    <title>{{env("APP_TITLE")}}</title>
    <meta name="description" content="{{ env('APP_DESCRIPTION', '-') }}">
    <meta name="keywords" content="{{ env('APP_KEYWORDS') }}">
    <meta name="author" content="{{ env('APP_AUTHOR', 'Ahmad Saparudin') }}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <link rel="shortcut icon" type="image/x-icon" href="{{asset('app-assets/images/ico/favicon.ico')}}">
    <link rel="manifest" href="{{asset('app-assets-custom\plugins\landing_telkom\img\favicon\site.webmanifes')}}">
    <link rel="mask-icon" href="{{asset('app-assets-custom\plugins\landing_telkom\img\favicon\safari-pinned-tab.svg')}}" color="#25237e">
    <meta name="msapplication-config" content="img/favicon/browserconfig.xml">
    <meta name="msapplication-TileColor" content="#ffaca3">
    <link rel="stylesheet" type="text/css" href="{{asset('app-assets-custom\plugins\landing_telkom\css\plugins.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('app-assets-custom\plugins\landing_telkom\css\loaders\loader.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('app-assets-custom\plugins\landing_telkom\css\main.css')}}">
    <meta name="theme-color" content="#272F7C">
    <meta name="msapplication-navbutton-color" content="#272F7C">
    <meta name="apple-mobile-web-app-status-bar-style" content="#272F7C">
  </head>

  <body class="overflow-hidden">
    <div class="loader">
      <div class="loader_content">
        <div class="loader-logo-container">
          <div class="loader-logo slideInDown">
            <img src="{{asset('app-assets-custom\plugins\landing_telkom\img\logo-original.png')}}" alt="Logo Tekom">
          </div>
          <div class="loader-caption slideInUp">
            <span class="loading-dots">
              <span class="dot"></span>
              <span class="dot"></span>
              <span class="dot"></span>
            </span>
          </div>
        </div>
      </div>
      <div class="loader_background"></div>
      <div class="loader_shape">
        <svg class="loader-transition-shape" width="100%" height="100%" viewbox="0 0 1440 800" preserveaspectratio="none">
          <path class="loader-morphing-path" d="M-22.4-87.7c-8.3,95.9,56.9,71.1,216.6,79c189.3,9.4,264.6-2.6,431.3-0.8c187.6,2,331.6-9.3,528.2-0.2c443.7,20.5,435.1-61.8,368.6-188.1C1455.9-324.1,34.5-746.7-22.4-87.7z"></path>
        </svg>
      </div>
    </div>

    <div id="fullpage">
      <div class="section fp-auto-height-responsive main-section" data-anchor="main">
        <div class="logo">
          <img src="{{asset('app-assets-custom\plugins\landing_telkom\img\logo.png')}}" alt="Logo Telkom">
        </div>
        <div class="main-section_content split fullheight">
          <div class="container-fluid p-0 fullheight">
            <div class="row no-gutters fullheight">
              <div class="col-12 col-xl-6 main-section_intro split fullheight">
                <div class="headline">
                  <h1>Creative<br>solutions</h1>
                  <span class="divider"></span>
                  <p>We are preparing something amazing and exciting for you. Special surprise for our subscribers only.</p>
                  <div class="btn-holder">
                    @if(Auth::guest())
                    <a href="{{Route('login_page')}}" class="btn btn-light">
                      <span class="btn-caption">Login</span>
                      <span class="icon ion-android-send"></span>
                    </a>
                    @else
                    <a href="{{Route('home')}}" class="btn btn-light">
                      <span class="btn-caption">Dashboard</span>
                      <span class="icon ion-android-send"></span>
                    </a>
                    @endif
                  </div>
                </div>
              </div>

              <div class="col-12 col-xl-6 main-section_media media-background fullheight">
                <div class="video-background">
                  <div class="" id="video-wrapper">
                    <!-- <div id="bgndVideo" class="player" data-property="{ videoURL:'https://youtu.be/Lhzo2DDTzu8', }"></div> -->
                  </div>
                </div>
                <div class="layer-blue-04"></div>
                <div class="side-shape-holder">
                  <svg class="main-side-shape-desktop" viewbox="0 0 100 600" style="enable-background:new 0 0 100 600;" preserveaspectratio="none">
                    <path id="main-side-path-desktop" class="main-side-path path-desktop" d="M0,0c0,0,28,21,28,44s-11,40-11,73s25,45,30,73s-29,53-29,85s64,36,64,80s-61,87-61,139c0,42,23,41,24,63c1.2,26-45,43-45,43V0z"></path>
                  </svg>
                  <svg class="main-side-shape-mobile" viewbox="0 0 600 100" style="enable-background:new 0 0 600 100;" preserveaspectratio="none">
                    <path id="main-side-path-mobile" class="main-side-path path-mobile" d="M600,0c0,0-21,28-44,28s-40-11-73-11s-45,25-73,30s-53-29-85-29s-36,64-80,64s-87-61-139-61c-42,0-41,23-63,24C17,46.2,0,0,0,0H600z"></path>
                  </svg>
                </div>
                <div class="credits split">
                  <p>&copy {{env('APP_COPY','Ahmad Saparudin')}} </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="section fp-auto-height-responsive content-section" data-anchor="about">
        <div class="container-fluid p-0 fullheight">
          <div class="row m-0 flex-xl-row-reverse fullheight">
            <div class="col-12 col-xl-6 p-0 content-section_info">
              <div class="content-block">
                <div class="section-title">
                  <h2>About Us</h2>
                  <span class="divider"></span>
                  <span class="subtitle">{!!$data->about!!}
                  <div class="btn-holder">
                    <a href="#portfolio" class="btn">
                      <span class="btn-caption">Hubungi Kami</span>
                      <span class="icon ion-ios-arrow-forward"></span>
                    </a>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-12 col-xl-6 p-0 content-section_media">
              <div class="image image-about"></div>
            </div>
          </div>
        </div>
      </div>
      <div class="section fp-auto-height-responsive content-section" data-anchor="blockquote">
        <div class="container-fluid p-0 fullheight">
          <div class="row m-0 flex-xl-row-reverse fullheight">
            <div class="col-12 col-xl-6 p-0 content-section_info">
              <div class="quote-object" id="quote-morphing">
                <svg class="quote-morphing-object" width="100%" height="100%" viewbox="0 0 600 600" xml:space="preserve">
                  <path class="quote-morphing-path" d="M585.1,354.1c0,148.3-151.7,234-300,234S13.6,448.3,13.6,300S178.8,13.6,327.1,13.6S585.1,205.8,585.1,354.1z"></path>
                </svg>
              </div>
              <div class="content-block">
                <div class="blockquote-content">
                  <blockquote cite="">
                    <i class="ion-quote"></i>
                    <p>{!!$data->blackquote_content!!}</p>
                    <span class="divider"></span>
                    <cite class="light-text-06">
                      <span>{!!$data->blackquote_author!!}</span>
                      <span>{!!$data->blackquote_title!!}</span>
                    </cite>
                  </blockquote>
                </div>
              </div>
            </div>
            <div class="col-12 col-xl-6 p-0 content-section_media">
              <div class="image image-quote"></div>
            </div>
          </div>
        </div>
      </div>
      <div class="section fp-auto-height-responsive content-section" data-anchor="contact">
        <div class="container-fluid p-0 fullheight">
          <div class="row no-gutters fullheight">
            <div class="col-12 col-xl-6 content-section_info">
              <div class="content-block">
                <div class="section-title">
                  <h2>Kontak</h2>
                  <span class="divider"></span>
                  <span class="subtitle">{!!$data->contact!!}</p>
                  <div class="btn-holder">
                    <a href="mailto:{{$data->email}}" id="writealine-trigger" class="btn">
                      <span class="btn-caption">Hubungi Kami</span>
                    </a>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-12 col-xl-6 content-section_media">
              <div class="image image-contact"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
    </div>
    <!-- Notify Me Popup End -->

    <div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="pswp__bg"></div>
      <div class="pswp__scroll-wrap">
        <div class="pswp__container">
            <div class="pswp__item"></div>
            <div class="pswp__item"></div>
            <div class="pswp__item"></div>
        </div>
        <div class="pswp__ui pswp__ui--hidden">
          <div class="pswp__top-bar">
            <div class="pswp__counter"></div>
            <button class="pswp__button pswp__button--close" title="Close (Esc)"></button>
            <button class="pswp__button pswp__button--share" title="Share"></button>
            <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>
            <div class="pswp__preloader">
              <div class="pswp__preloader__icn">
                <div class="pswp__preloader__cut">
                  <div class="pswp__preloader__donut"></div>
                </div>
              </div>
            </div>
          </div>
          <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
            <div class="pswp__share-tooltip"></div>
          </div>
          <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)"></button>
          <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)"></button>
          <div class="pswp__caption">
            <div class="pswp__caption__center"></div>
          </div>
        </div>
      </div>
    </div>
    <script src="{{asset('app-assets-custom\plugins\landing_telkom\js\libs.min.js')}}"></script>
    <script src="{{asset('app-assets-custom\plugins\landing_telkom\js\gallery-init.js')}}"></script>
    <script src="{{asset('app-assets-custom\plugins\landing_telkom\js\punchy-custom.js')}}"></script>
  </body>
</html>
