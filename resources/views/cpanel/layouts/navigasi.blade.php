<div class="main-menu-content">
  <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
    @foreach($menu as $index_header => $header_menu)
        @if($header_menu->header_name!="" || $header_menu->header_name!=null)
        <!-- Bagian Header Navigasi -->
        <li class=" navigation-header">
          <span data-i18n="nav.category.header_{{$index_header}}">{{$header_menu->header_name}}</span><i class="la la-ellipsis-h ft-minus" data-toggle="tooltip"
          data-placement="right" data-original-title="Layouts"></i>
        </li>
        <!-- Bagian Header Navigasi -->
        @endif
        @foreach($header_menu->menu_parent as $index_parent => $parent_menu)
        <li class=" nav-item">
            <a href="#">
                <i class="{{$parent_menu->icon}}"></i>
                <span class="menu-title" data-i18n="nav.parent_{{$index_parent}}.main">{{$parent_menu->parent_name}}</span>
            </a>
            <ul class="menu-content">
            @foreach($parent_menu->menu_child as $menu_child)
            <li class="{{set_active($menu_child->route_name)}}">
                <a class="menu-item" href="{{url($menu_child->url)}}" data-i18n="nav.parent_{{$index_parent}}.child_{{$menu_child->id_menu}}">{{$menu_child->menu_name}}</a>
            </li>
            @endforeach
            </ul>
        </li>
        @endforeach
    @endforeach
  </ul>
</div>