<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<head>
  @include("cpanel.layouts.default-header-links")
  @yield("plugin")
</head>
<body class="vertical-layout vertical-menu 2-columns   menu-expanded fixed-navbar"
data-open="click" data-menu="vertical-menu" data-col="2-columns">
  @include("cpanel.layouts.header")
  <div class="main-menu menu-fixed menu-light menu-accordion menu-shadow" data-scroll-to-active="true">
    @include("cpanel.layouts.navigasi")
  </div>
  <div class="app-content content">
    <div class="content-wrapper">
      <div class="content-header row">
        @yield("link")
      </div>
      <div class="content-body">
        @yield("konten")
      </div>
    </div>
  </div>
  @include("cpanel.layouts.footer")
  @include("cpanel.layouts.default-scripts")
  @yield("script")
</body>
</html>