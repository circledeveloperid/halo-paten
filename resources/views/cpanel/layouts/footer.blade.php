<footer class="footer footer-static footer-light navbar-border navbar-shadow">
	<p class="clearfix blue-grey lighten-2 text-sm-center mb-0 px-2">
	  <span class="float-md-left d-block d-md-inline-block">&copy {{env('APP_COPY', 'Ahmad Saparudin, 2019')}}</span>
	  <span class="float-md-right d-block d-md-inline-blockd-none d-lg-block"> Didedikasikan untuk {{env('APP_DISTRIBUTED_FOR', 'Universitas Jambi')}}</span>
	</p>
</footer>