@extends("cpanel.layouts.app")

@section("konten")

@endsection

@section("plugin")
<link rel="stylesheet" type="text/css" href="{{asset('app-assets/vendors/css/tables/datatable/datatables.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('modules/daterangepicker/daterangepicker.css')}}" />
@endsection

@section("script")
<script src="{{asset('app-assets/vendors/js/tables/datatable/datatables.min.js')}}" type="text/javascript"></script>
<script type="text/javascript" src="{{asset('modules/daterangepicker/moment.min.js')}}"></script>
<script type="text/javascript" src="{{asset('modules/daterangepicker/daterangepicker.min.js')}}"></script>
@endsection