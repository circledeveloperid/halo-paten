<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>HALOPATEN - Sistem Pendaftaran Merek</title>
    @include("layouts.component.header-link")
    @yield("css-tambahan")
</head>

<body>
    <div class="page-wrapper">
        <!-- Preloader -->
        <div class="preloader"></div>
        
        <!-- Main Header-->
        @include("layouts.component.header")
        <!--End Main Header -->

        <!--Page Title-->
        <!-- <section class="page-title" style="background-image: url({{asset('assets/template/images/background/7.jpeg')}});">
            <div class="auto-container">
                <div class="inner-container clearfix">
                    <h1>Latest News</h1>
                    <ul class="bread-crumb clearfix">
                        <li><a href="index-1.html">Home</a></li>
                        <li>Blogs</li>
                    </ul>
                </div>
            </div>
        </section> -->
        <!--End Page Title-->

         <!-- Sidebar Page Container -->
        <div class="sidebar-page-container">
            <div class="auto-container block-content">
                @yield("content")
            </div>
        </div>
        <!-- End Sidebar Container -->
        @include("layouts.component.footer")
    </div>
    <div class="scroll-to-top scroll-to-target" data-target="html">
        <span class="fa fa-angle-double-up"></span>
    </div>
    @include("layouts.component.footer-js")
    @include("layouts.component.chat")
    @yield("js-tambahan")
</body>
</html>