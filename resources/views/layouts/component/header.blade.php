<header class="main-header">

    <!-- Header Upper -->
    <div class="header-upper">
        <div class="auto-container">
            <div class="clearfix">
                
                <div class="logo-outer">
                    <div class="logo logo-padding">
                        <img src="{{asset('assets\default\logo.png')}}" alt="" title="" style="height:120px">
                    </div>
                    <h4 class="white hideOnDesktop bold" style="margin-top:15px">HALOPATEN</h4>
                </div>
                
                <div class="classForDesktop">
                    <div class="row" style="margin-bottom:5px">
                        <div class="col-md-12">
                            <h5 class="white bold">Solusi Pendaftaran Merek Secara Cepat & Profesional</h5>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <p class="white">
                                <img src="{{asset('assets/icon/mini/icon_check.png')}}" width="20px" height="20px"> Gratis biaya pengecekan merek
                            </p>
                            <p class="white">
                                <img src="{{asset('assets/icon/mini/icon_check.png')}}" width="20px" height="20px"> Garansi 100 persen uang kembali
                            </p>
                            <p class="white">
                                <img src="{{asset('assets/icon/mini/icon_check.png')}}" width="20px" height="20px"> Proses cepat & mudah
                            </p>
                            <p class="white">
                                <img src="{{asset('assets/icon/mini/icon_check.png')}}" width="20px" height="20px"> Harga transparan & tanpa hidden cost
                            </p>
                        </div>
                        <div class="col-md-6">
                            <p class="white">
                                <img src="{{asset('assets/icon/mini/icon_check.png')}}" width="20px" height="20px"> Terbuka untuk umum
                            </p>
                            <p class="white">
                                <img src="{{asset('assets/icon/mini/icon_check.png')}}" width="20px" height="20px"> Terbuka untuk semua jenis usaha
                            </p>
                            <p class="white">
                                <img src="{{asset('assets/icon/mini/icon_check.png')}}" width="20px" height="20px"> Melayani 24 jam/7hari non-stop
                            </p>
                            <p class="white">
                                <img src="{{asset('assets/icon/mini/icon_check.png')}}" width="20px" height="20px"> Melayani klien seluruh Indonesia
                            </p>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <!-- Header Lower -->
    <div class="header-lower bg-black">
        <div class="auto-container">
            <div class="inner-container clearfix">
                <div class="">
                    <!-- Main Menu -->
                    <nav class="main-menu navbar-expand-md ">
                        <div class="navbar-header">
                            <!-- Toggle Button -->      
                            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                                <span class="icon fa fa-bars"></span>
                            </button>
                        </div>
                        
                        <div class="collapse navbar-collapse clearfix" id="navbarSupportedContent">
                            <ul class="navigation clearfix">
                                <li>
                                    <a href="{{route('page')}}/home">Home</a>
                                </li>
                                <li class="dropdown"><a href="#">Layanan</a>
                                    <ul>
                                        <li><a href="{{route('permohonan-user.create')}}">Permohonan Merek Baru</a></li>
                                        <li><a href="{{route('comming_service')}}">Perpanjangan Merek</a></li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="{{route('page')}}/tentang">Tentang Kami</a>
                                </li>
                                <li>
                                    <a href="{{route('page')}}/faq">FAQ</a>
                                </li>
                                <li>
                                    <a href="{{route('page')}}/kontak">Kontak</a>
                                </li>
                                @include("layouts.component.menu_user_on_mobile")
                                <!-- <li class="current dropdown"><a href="#">Blog</a>
                                    <ul>
                                        <li><a href="blog.html">Blog</a></li>
                                        <li><a href="blog-detail.html">Blog Detail</a></li>
                                    </ul>
                                </li> -->
                            </ul>
                            @include("layouts.component.menu_user")
                        </div>
                    </nav>
                    <!-- Main Menu End-->
                </div>
            </div>
        </div>
    </div>
    <!--End Header Lower-->
    
    <!-- Sticky Header -->
</header>