<script src="{{asset('assets\template\js\jquery.js')}}"></script> 
<script src="{{asset('assets\template\js\popper.min.js')}}"></script>
<script src="{{asset('assets\template\js\bootstrap.min.js')}}"></script>
<script src="{{asset('assets\template\js\jquery.fancybox.js')}}"></script>
<script src="{{asset('assets\template\js\owl.js')}}"></script>
<script src="{{asset('assets\template\js\wow.js')}}"></script>
<script src="{{asset('assets\template\js\appear.js')}}"></script>
<script src="{{asset('assets\template\js\mixitup.js')}}"></script>
<script src="{{asset('assets\template\js\script.js')}}"></script>
<script src="{{asset('assets\template\js\color-settings.js')}}"></script>
@include("additional.js.default")