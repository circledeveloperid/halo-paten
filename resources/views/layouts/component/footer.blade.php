<section class="bg-black no-padding-bottom fix-padding-top footer-padding">
    <div class="auto-container">
        <div class="row">
            <div class="col-md-3 hideOnMobile">
                <p class="white">Merek</p>
                <p class="white">Paten</p>
                <p class="white">Hak Cipta</p>
                <p class="white">Desain Industri</p>
                <p class="white">Indikasi Geografis</p>
                <p class="white">DTLST & Rahasia Dagang</p>
            </div>
            <div class="col-md-3">
                <p class="white">Office:</p>
                <p class="white">CoHive Cental Park APL Tower 25th Floor, Jl. Letjen S. Parman Kav. 28, Grogol Petamburan, Jakarta 11470</p>
                <p class="white">Email: cs@halopaten.com</p>
            </div>
            <div class="col-md-2 hideOnMobile">
                <p><a href="{{route('page')}}/home" class="white">Home</a></p>
                <p><a href="#" class="white">Layanan</a></p>
                <p><a href="{{route('page')}}/tentang" class="white">Tentang Kami</a></p>
                <p><a href="{{route('page')}}/faq" class="white">FAQ</a></p>
                <p><a href="{{route('page')}}/kontak" class="white">Kontak</a></p>
            </div>
            <div class="col-md-4 hideOnMobile">
                <div class="row bank_icon">
                    <div class="col-md-3">
                        <img src="{{asset('assets\icon\bank\bca.png')}}" alt="">
                    </div>
                    <div class="col-md-3">
                        <img src="{{asset('assets\icon\bank\mandiri.png')}}" alt="">
                    </div>
                    <div class="col-md-3">
                        <img src="{{asset('assets\icon\bank\atm-bersama.png')}}" alt="">
                    </div>
                    <div class="col-md-3">
                        <img src="{{asset('assets\icon\bank\permata.png')}}" alt="">
                    </div>
                </div>
                <div class="row bank_icon">
                    <div class="col-md-3">
                        <img src="{{asset('assets\icon\bank\bri.png')}}" alt="">
                    </div>
                    <div class="col-md-3">
                        <img src="{{asset('assets\icon\bank\bni.png')}}" alt="">
                    </div>
                    <div class="col-md-3">
                        <img src="{{asset('assets\icon\bank\panin.png')}}" alt="">
                    </div>
                    <div class="col-md-3">
                        <img src="{{asset('assets\icon\bank\cimb.png')}}" alt="">
                    </div>
                </div>
                <div class="row bank_icon">
                    <div class="col-md-3">
                        <img src="{{asset('assets\icon\bank\visa.png')}}" alt="">
                    </div>
                    <div class="col-md-3">
                        <img src="{{asset('assets\icon\bank\mastercard.png')}}" alt="">
                    </div>
                    <div class="col-md-3">
                        <img src="{{asset('assets\icon\bank\jcb.png')}}" alt="">
                    </div>
                    <div class="col-md-3">
                        <img src="{{asset('assets\icon\bank\ovo.png')}}" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <footer class="main-footer no-padding-top bg-black">
        <div class="auto-container">
            <hr style="border:1px solid grey">
            <p class="white">Hak Cipta &copy {{date('Y')}} HALOPATEN.COM</p>
            <br>
        </div>
    </footer>
</section>