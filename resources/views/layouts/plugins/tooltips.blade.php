<link rel="stylesheet" type="text/css" href="{{asset('assets/plugins/tooltips/dist/css/plugins/tooltipster/sideTip/themes/tooltipster-sideTip-light.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('assets/plugins/tooltips/dist/css/tooltipster.bundle.min.css')}}">
<script type="text/javascript" src="{{asset('assets/plugins/tooltips/dist/js/tooltipster.bundle.min.js')}}"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('.tooltip').tooltipster({
        	theme: 'tooltipster-light',
        	side: 'bottom',
        	maxWidth: '200',
        });
    });
</script>