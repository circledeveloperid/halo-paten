@extends("layouts.template")

@section("content")
<div class="row">
    <div class="col-md-12">
        <h5 class="bold">Oops! Layanan ini belum tersedia</h5>
    </div>
</div>
@endsection

@section("css-tambahan")
@endsection

@section("js-tambahan")
@endsection